--Câu 1: Đoạn lệnh dưới đây thống kê số lượng đơn đặt hàng và doanh thu của từng ngày
--trong tháng @month năm @year

  
--Yêu cầu: lập trình để kết quả thống kê hiển thị số lượng đơn đặt hàng và 
-- doanh thu của tất cả các ngày trong tháng @month năm @year

declare @month int = 1,
            @year int = 2018;

	declare @tblOrderDate table
	(
		OrderDate date primary key
	);
	declare @date date = DATEFROMPARTS(@year, @month, 1);

	while (DAY(@date) <= Day(EOMONTH(@date)))
		 if MONTH(@date) > @month
			begin
				break
			end
		else
			begin
				insert into @tblOrderDate values (@date);
				set @date = DATEADD(DAY, 1, @date);
			end
	select	t1.OrderDate, ISNULL(t2.CountOfOrders, 0) as CountOfOrders , ISNULL(t2.Revenue, 0) as Revenue
	from @tblOrderDate as t1
	left join(
	select	o.OrderDate,
			count(distinct o.OrderId) as CountOfOrders, 
			sum(od.Quantity * od.SalePrice) as Revenue
	from	Orders as o
			join OrderDetails as od on o.OrderId = od.OrderId 
	where	month(o.OrderDate) = @month and year(o.OrderDate) = @year
	group by o.OrderDate
	) as t2 on t1.OrderDate = t2.OrderDate
	go


--Lưu ý một số hàm kiểu ngày có thể cần sử dụng:
--- Hàm DATEADD
		SELECT	DATEADD(DAY, -1, '2021/09/20'),
				DATEADD(DAY, 1, '2021/09/20'),
				DATEADD(YEAR, -1, '2021/09/20'),
				DATEADD(YEAR, 1, '2021/09/20')
		GO
--- Hàm DATEFROMPARTS
		SELECT	DATEFROMPARTS(2021, 9, 20)
		GO
--- Hàm DATEDIFF
		SELECT	DATEDIFF(DAY, '2021/9/20', '2021/9/21'),
				DATEDIFF(DAY, '2021/9/20', '2021/9/19')	
		GO

-- Câu 2: Cho bảng dữ liệu có cấu trúc sau:
create table TheoDoiVatTu
(
    Thang int primary key,  -- Tháng theo dõi
    TonDauThang int,        -- Số lượng vật tư còn tồn vào đầu tháng
    NhapTrongThang int,     -- Số lượng vật tư được nhập trong tháng
    XuatTrongThang int,     -- Số lượng vật tư được xuất trong tháng
    TonCuoiThang int        -- Số lượng vật tư còn tồn vào cuối tháng
)
go
-- Dữ liệu trong bảng như sau:
insert into TheoDoiVatTu
values   (1, 550, 30, 20, null),
        (2, null, 10, 0, null),
        (3, null, 100, 120, null),
        (4, null, 100, 0, null),
        (5, null, 40, 200, null),
        (6, null, 0, 10, null),
        (7, null, 30, 15, null),
        (8, null, 0, 30, null),
        (9, null, 150, 100, null),
        (10, null, 200, 50, null),
        (11, null, 0, 40, null),
        (12, null, 50, 200, null)

select * from TheoDoiVatTu
-- Yêu cầu: Tính toán và cập nhật giá trị của các cột TonDauThang và TonCuoiThang trong bảng trên

declare contro cursor for select TonDauThang, TonCuoiThang, NhapTrongThang, XuatTrongThang from TheoDoiVatTu

open contro
declare @tonDT int, @tonCT int, @nhapTT int, @xuatTT int 
fetch next from contro into  @tonDT, @tonCT, @nhapTT , @xuatTT  


declare @thang int = 1
declare @tonDTPrev int, @tonCTPrev int;

set @tonDTPrev = @tonDT
set @tonCTPrev = @tonCT

while(@@FETCH_STATUS = 0) 
		begin
			if(@tonDT is null)
				begin
					set @tonDT =  @tonCTPrev
					set @tonDTPrev = @tonCT	
					UPDATE TheoDoiVatTu
					SET TonDauThang = @tonDT
					Where Thang = @thang	
				end
			if(@tonCT is null)
				begin
					set @tonCT = @tonDTPrev 		
					set @tonCTPrev = @tonDT + (@nhapTT - @xuatTT)
					UPDATE TheoDoiVatTu
					SET TonCuoiThang = @tonDT + (@nhapTT - @xuatTT)
					Where Thang = @thang
				end
			set @thang = @thang + 1
			fetch next from contro into  @tonDT, @tonCT, @nhapTT , @xuatTT  	
		end
close contro
deallocate contro
go

