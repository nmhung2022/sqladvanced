﻿				
-----------------------------------------------------------------------------------------------
--2. Hãy cho biết chương trình đào tạo dự kiến của mỗi khóa ngành có tổng số tín chỉ là bao nhiêu,
--  bao nhiêu tín chỉ bắt buộc, bao nhiêu tín chỉ tự chọn 
-----------------------------------------------------------------------------------------------
declare @t1 table (MaChuongTrinhDaoTao nvarchar(50), TenChuongTrinhDaoTao nvarchaR(255), MaNganh nvarchar(50), TenNganh nvarchar(255),SoTinChi int)
declare @t2 table (MaChuongTrinhDaoTao nvarchar(50), TenChuongTrinhDaoTao nvarchaR(255), MaNganh nvarchar(50), TenNganh nvarchar(255),SoTinChiBatBuoc int)
declare @t3 table (MaChuongTrinhDaoTao nvarchar(50), TenChuongTrinhDaoTao nvarchaR(255), MaNganh nvarchar(50), TenNganh nvarchar(255),SoTinChiTuDo int)

insert into @t1(MaChuongTrinhDaoTao, TenChuongTrinhDaoTao, MaNganh, TenNganh, SoTinChi)
select ctdt.MaChuongTrinhDaoTao, TenChuongTrinhDaoTao, ndt.MaNganh, TenNganh, sum(hp.SoTinChi)
from ChuongTrinhDaoTao as ctdt join NoiDungChuongTrinhDaoTao as ndctdt on ctdt.MaChuongTrinhDaoTao = ndctdt.MaChuongTrinhDaoTao
								join HocPhan as hp on ndctdt.MaHocPhan = hp.MaHocPhan
								join NganhDaoTao as ndt on ctdt.MaNganh = ndt.MaNganh
group by ctdt.MaChuongTrinhDaoTao, TenChuongTrinhDaoTao, ndt.MaNganh, TenNganh

insert into @t2(MaChuongTrinhDaoTao, TenChuongTrinhDaoTao, MaNganh, TenNganh, SoTinChiBatBuoc)
select ctdt.MaChuongTrinhDaoTao, TenChuongTrinhDaoTao, ndt.MaNganh, TenNganh, sum(hp.SoTinChi)
from ChuongTrinhDaoTao as ctdt join NoiDungChuongTrinhDaoTao as ndctdt on ctdt.MaChuongTrinhDaoTao = ndctdt.MaChuongTrinhDaoTao
								join HocPhan as hp on ndctdt.MaHocPhan = hp.MaHocPhan
								join NganhDaoTao as ndt on ctdt.MaNganh = ndt.MaNganh
where HocPhanBatBuoc = 1
group by ctdt.MaChuongTrinhDaoTao, TenChuongTrinhDaoTao, ndt.MaNganh, TenNganh

insert into @t3(MaChuongTrinhDaoTao, TenChuongTrinhDaoTao, MaNganh, TenNganh, SoTinChiTuDo)
select ctdt.MaChuongTrinhDaoTao, TenChuongTrinhDaoTao, ndt.MaNganh, TenNganh, sum(hp.SoTinChi)
from ChuongTrinhDaoTao as ctdt join NoiDungChuongTrinhDaoTao as ndctdt on ctdt.MaChuongTrinhDaoTao = ndctdt.MaChuongTrinhDaoTao
								join HocPhan as hp on ndctdt.MaHocPhan = hp.MaHocPhan
								join NganhDaoTao as ndt on ctdt.MaNganh = ndt.MaNganh
where HocPhanBatBuoc = 0
group by ctdt.MaChuongTrinhDaoTao, TenChuongTrinhDaoTao, ndt.MaNganh, TenNganh

select t1.*, t2.SoTinChiBatBuoc, t3.SoTinChiTuDo
from @t1 as t1 join @t2 as t2 on t1.MaChuongTrinhDaoTao = t2.MaChuongTrinhDaoTao
				join @t3 as t3 on t1.MaChuongTrinhDaoTao = t3.MaChuongTrinhDaoTao
				
-----------------------------------------------------------------------------------------------
--3. Thống kê số lượng sinh viên nhập học của từng đơn vị theo từng khóa (mỗi đơn  vị trong từng 
-- tuyển sinh có bao nhiêu sinh viên nhập học) khóa (có tổ chức) 
-----------------------------------------------------------------------------------------------



select kn.MaKhoaHoc as mkh, dv.MaDonVi, count(hs.MaSinhVien) as SoLuongSV
from SinhVien as sv join HoSoNhapHoc as hs on sv.MaSinhVien = hs.MaSinhVien
					join KhoaHoc_NganhDaoTao as kn on hs.MaNganh = kn.MaNganh
					join NganhDaoTao as n on kn.MaNganh = n.MaNganh
					join DonVi as dv on n.MaDonVi = dv.MaDonVi
group by kn.MaKhoaHoc, dv.MaDonVi

-----------------------------------------------------------------------------------------------
--4. Thống kê số lượng sinh viên nhập học của các ngành thuộc khóa tuyển sinh năm  @namtuyensinh 
-----------------------------------------------------------------------------------------------
if(exists(select * from sys.objects where name = 'proc_Cau4'))
	drop proc proc_Cau3;
go
create proc proc_Cau3 
(
	@namtuyensinh int
)
as
begin
	set nocount on;
	select ndt.MaNganh, ndt.TenNganh, isnull(t.s1, 0) as SoLuongSinhVien
	from NganhDaoTao as ndt left join
		(
			select hs.MaNganh, count(hs.MaSinhVien) as s1
			from NganhDaoTao as ndt join KhoaHoc_NganhDaoTao as khndt on ndt.MaNganh = khndt.MaNganh
									join HoSoNhapHoc as hs on khndt.MaNganh = hs.MaNganh
									join KhoaHoc as kh on khndt.MaKhoaHoc = kh.MaKhoaHoc
			where kh.NamTuyenSinh = @namtuyensinh
			group by hs.MaNganh	
		) as t on ndt.MaNganh = t.MaNganh
end
go

-- Test thủ tục
exec  proc_Cau3
	@namtuyensinh = 2010
			
-----------------------------------------------------------------------------------------------
--5. Trong học kỳ @hocky năm học @namhoc, những lớp học phần nào có số lượng  sinh viên đăng ký chưa đủ số lượng tối thiểu. 
-----------------------------------------------------------------------------------------------
if(exists(select * from sys.objects where name = 'proc_Cau5'))
	drop proc proc_Cau5;
go
create proc proc_Cau5 
(
	@hocky int,
	@namhoc nvarchar(50)
)
as
	begin
	select l.MaLopHocPhan, TenLopHocPhan, (SoSinhVienToiThieu), count(hs.MaHocTap)
	from LopHocPhan as l join LopHocPhan_DanhSachSinhVien as ld on l.MaLopHocPhan = ld.MaLopHocPhan
						 join HoSoNhapHoc as hs on ld.MaHocTap = hs.MaHocTap
	where SUBSTRING(MaHocKy, 11, 10) = @hocky and SUBSTRING(MaHocKy, 1, 9) = @namhoc
	group by l.MaLopHocPhan, TenLopHocPhan, (SoSinhVienToiThieu)
	having count(hs.MaSinhVien) < SoSinhVienToiThieu

end
go

-- Test thủ tục
exec proc_Cau5 
		@hocky = 1,
		@namhoc =N'2010-2011'


				
-----------------------------------------------------------------------------------------------
--6. Thống kê số lượng giờ mà mỗi giáo viên phải dạy trong năm học @namhoc (chỉ tính trong học kỳ 1 và học kỳ 2) 
-----------------------------------------------------------------------------------------------
if(exists(select * from sys.objects where name = 'proc_Cau6'))
	drop proc proc_Cau6;
go
create proc proc_Cau6 
(
	@namhoc nvarchar(50)
)
as
	begin
	select gv.MaGV, HoDem, Ten, sum(SoGioDay)
	from LopHocPhan as l join LopHocPhan_PhanCongDay as lp on l.MaLopHocPhan = lp.MaLopHocPhan
						 join GiaoVien as gv on gv.MaGV = lp.MaGV
	where (SUBSTRING(MaHocKy,11,10) = 1 or SUBSTRING(MaHocKy,11,10) = 2) and SUBSTRING(MaHocKy,1,9) = @namhoc
	group by gv.MaGV, HoDem, Ten

end
go

-- Test thủ tục
exec proc_Cau6
		@namhoc =N'2010-2011'
		
-----------------------------------------------------------------------------------------------
--7. Những giáo viên nào trong năm học @namhoc có số giờ dạy vượt chuẩn (chỉ tính  trong học kỳ 1 và học kỳ 2) 
-----------------------------------------------------------------------------------------------

if(exists(select * from sys.objects where name = 'proc_Cau7'))
	drop proc proc_Cau7;
go
create proc proc_Cau7 
(
	@namhoc nvarchar(50)
)
as
	begin
	select gv.MaGV, HoDem, Ten, gv.DinhMucGioChuan, sum(SoGioDay) as TongSoGioDay, SUBSTRING(MaHocKy, 11, 1) as HocKy
	from LopHocPhan as l join LopHocPhan_PhanCongDay as lp on l.MaLopHocPhan = lp.MaLopHocPhan
						 join GiaoVien as gv on gv.MaGV = lp.MaGV
	where (SUBSTRING(MaHocKy,11,1) = 1 or SUBSTRING(MaHocKy,11,10) = 2) and SUBSTRING(MaHocKy,1,9) = @namhoc
	group by gv.MaGV, HoDem, Ten, gv.DinhMucGioChuan, SUBSTRING(MaHocKy, 11, 1)
	having sum(SoGioDay) > gv.DinhMucGioChuan

end
go

-- Test thủ tục
exec proc_Cau7
	@namhoc = N'2010-2011'

-----------------------------------------------------------------------------------------------
--8. Trong học kỳ @hocky năm học @namhoc, mỗi một đơn vị phải phụ trách bao  nhiêu học phần, bao nhiêu lớp học phần? 
-----------------------------------------------------------------------------------------------
if(exists(select * from sys.objects where name = 'proc_Cau8'))
	drop proc proc_Cau8;
go
create proc proc_Cau8 
(
	@hocky int,
	@namhoc nvarchar(50)
)
as
begin
	
		select dv.MaDonVi, count(hp.MaHocPhan) as SoHocPhan, count(lp.MaLopHocPhan) as SoLopHocPhan
		from DonVi as dv left join HocPhan as hp on dv.MaDonVi = hp.MaDonVi
						 left join GiaoVien as gv on gv.MaDonVi = dv.MaDonVi
						 left join LopHocPhan_PhanCongDay as lp on gv.MaGV = lp.MaGV
		where SUBSTRING(MaLopHocPhan,11,1) = @hocky and SUBSTRING(MaLopHocPhan,1,9) = @namhoc
		group by dv.MaDonVi
	
end
go

-- Test thủ tục
exec proc_Cau8
	@hocky = 1,
	@namhoc =N'2010-2011'
-----------------------------------------------------------------------------------------------
--9. Thống kê số lượng sinh viên học lại, học cải thiện (tức là học lần thứ 2 trở đi) của  các lớp học phần được 
-- mở trong học kỳ @hocky năm học @namhoc.
-----------------------------------------------------------------------------------------------
if(exists(select * from sys.objects where name = 'proc_Cau9'))
	drop proc proc_Cau9;
go
create proc proc_Cau9 
(
	@hocky int,
	@namhoc nvarchar(50)
)
as
begin
	select lhp.MaLopHocPhan,lhp.TenLopHocPhan, count(ld.MaHocTap) as SoSinhVienHocLai
	from LopHocPhan as lhp join LopHocPhan_DanhSachSinhVien as ld on lhp.MaLopHocPhan = ld.MaLopHocPhan
	where (SUBSTRING(lhp.MaHocKy, 11, 1) = @hocky and SUBSTRING(lhp.MaHocKy, 1, 9) = @namhoc) and ld.LanHoc >= 2
	group by lhp.MaLopHocPhan, TenLopHocPhan
end
go

-- Test thủ tục
exec proc_Cau9
	@hocky = 2, 
	@namhoc = N'2010-2011'

-----------------------------------------------------------------------------------------------
--10.Thống kê xem mỗi sinh viên của khóa @MaKhoaHoc ngành @MaNganh trong học  
-- kỳ @hocky năm học @namhoc đăng ký học bao nhiêu lớp học phần, tổng số tín chỉ đăng ký là bao nhiêu, 
-- trong đó có bao nhiêu tín chỉ sinh viên đăng ký học lại/cải thiện. 
-----------------------------------------------------------------------------------------------
if(exists(select * from sys.objects where name ='proc_Cau10'))
    drop proc proc_Cau10
go
create proc proc_Cau10
(
    @MaHocKy nvarchar(50),
    @MaKhoaHoc nvarchar(50),
    @MaNganh nvarchar(50)
)
as
begin
    set nocount on;

    select sv.MaSinhVien, sv.HoDem,sv.Ten,count(hp.MaHocPhan) as N'Tổng Số Học Phần',sum(hp.SoTinChi) as N'Số Tín Chỉ Đã Đăng Ký',
    sum(case when lhpds.LanHoc > 1 then hp.SoTinChi else 0 end) as N'Học Cải Thiện'
     from HocPhan as hp 
                join LopHocPhan as lhp on hp.MaHocPhan = lhp.MaHocPhan
                join LopHocPhan_DanhSachSinhVien as lhpds on lhp.MaLopHocPhan = lhpds.MaLopHocPhan
                join HoSoNhapHoc as hs on hs.MaHocTap = lhpds.MaHocTap
                join SinhVien as sv on hs.MaSinhVien = sv.MaSinhVien
                where hs.MaKhoaHoc = @MaKhoaHoc and hs.MaNganh = @MaNganh and lhp.MaHocKy = @MaHocKy
                group by sv.MaSinhVien, sv.HoDem,sv.Ten 
end
go

-- Test thủ tục
exec proc_Cau10 
	@MaHocKy = '2010-2011.1',
    @MaKhoaHoc =  'K4',
    @MaNganh = 'K4'


-----------------------------------------------------------------------------------------------
--11.Hãy cho biết, trong năm học @namhoc khối lượng giảng dạy trung bình của giảng  viên mỗi khoa 
-- là bao nhiêu giờ (khối lượng giảng dạy trung bình = tổng số giờ dạy của giáo viên/tổng số giáo viên của khoa) 
-----------------------------------------------------------------------------------------------
if(exists(select * from sys.objects where name = 'proc_Cau11'))
	drop proc proc_Cau11;
go
create proc proc_Cau11 
(
	@namhoc nvarchar(50)
)
as
begin

		select dv.MaDonVi, dv.TenDonVi, (sum(lp.SoGioDay)/count(gv.MaGV)) as KhoiLuongGiangDayTrungBinh
		from GiaoVien as gv left join LopHocPhan_PhanCongDay as lp on gv.MaGV = lp.MaGV
							join DonVi as dv on gv.MaDonVi = dv.MaDonVi
		where SUBSTRING(MaLopHocPhan, 1, 9) = @namhoc and dv.TenDonVi like 'Khoa%'
		group by dv.MaDonVi, TenDonVi
	
end
go

-- Test thủ tục
exec proc_Cau11 
	@namhoc = N'2010-2011'


-----------------------------------------------------------------------------------------------
--12.Trong khóa tuyển sinh năm @namtuyensinh, ngành nào có số lượng sinh viên  nhập học nhiều nhất, 
-- là bao nhiêu sinh viên? (chỉ tính sinh viên học ngành chính) 13.Trong năm học @namhoc, 
-- những giáo viên nào có tổng số giờ dạy nhiều nhất, là  bao nhiêu giờ? 
-----------------------------------------------------------------------------------------------
if(exists(select * from sys.objects where name = 'proc_Cau12'))
	drop proc proc_Cau12;
go
create proc proc_Cau12
( 
	@namtuyensinh int
)
as
begin
	select n.MaNganh, n.TenNganh, count(hs.MaHocTap)
	from NganhDaoTao as n join KhoaHoc_NganhDaoTao as kn on n.MaNganh = kn.MaNganh
		join HoSoNhapHoc as hs on kn.MaNganh = hs.MaNganh
			join KhoaHoc as kh on kn.MaKhoaHoc = kh.MaKhoaHoc
	where NamTuyenSinh = @namtuyensinh
	group by n.MaNganh, n.TenNganh
	having count(hs.MaHocTap) >= all 
		(select count(hs.MaHocTap)
			from NganhDaoTao as n join KhoaHoc_NganhDaoTao as kn on n.MaNganh = kn.MaNganh
					join HoSoNhapHoc as hs on kn.MaNganh = hs.MaNganh
						join KhoaHoc as kh on kn.MaKhoaHoc = kh.MaKhoaHoc
			where NamTuyenSinh = @namtuyensinh
			group by n.MaNganh
		)
end
go

-- Test thủ tục
exec proc_Cau12 
	@namtuyensinh = 2010

-----------------------------------------------------------------------------------------------
-- 13.Trong năm học @namhoc, những giáo viên nào có tổng số giờ dạy nhiều nhất, là  bao nhiêu giờ? 
-----------------------------------------------------------------------------------------------
if(exists(select * from sys.objects where name = 'proc_cau13'))
	drop proc proc_cau13;
go
create proc proc_cau13
( 
	@namhoc nvarchar(50)
)
as
begin
	select lp.MaGV,gv.HoDem, Ten, sum(SoGioDay)
	from GiaoVien as gv join LopHocPhan_PhanCongDay as lp on gv.MaGV = lp.MaGV
	where substring(MaLopHocPhan,1,9) = @namhoc
	group by lp.MaGV, HoDem, Ten
	having sum(SoGioDay) >= all 
	(select sum(SoGioDay) from GiaoVien as gv join LopHocPhan_PhanCongDay as lp on gv.MaGV = lp.MaGV
		where substring(MaLopHocPhan,1,9) = @namhoc
			group by lp.MaGV, HoDem, Ten
	)

end
go

-- Test thủ tục
exec proc_cau13 @namhoc = N'2010-2011'

-----------------------------------------------------------------------------------------------
-- 14. Thống kê số lượng sinh viên nhập học của từng ngành của các khóa kết quả hiển  thị theo mẫu sau:
-----------------------------------------------------------------------------------------------
select n.MaNganh, n.TenNganh,
			 count(case when kh.MaKhoaHoc = 'K0' then hs.MaHocTap else 0 end) K0,
			 count(case when kh.MaKhoaHoc = 'K10' then hs.MaHocTap else 0 end) K10,
			 count(case when kh.MaKhoaHoc = 'K11' then hs.MaHocTap else 0 end) K11,
			 count(case when kh.MaKhoaHoc = 'K2' then hs.MaHocTap else 0 end) K2,
			 count(case when kh.MaKhoaHoc = 'K3' then hs.MaHocTap else 0 end) K3,
			 count(case when kh.MaKhoaHoc = 'K4' then hs.MaHocTap else 0 end) K4,
			 count(case when kh.MaKhoaHoc = 'K5' then hs.MaHocTap else 0 end) K5,
			 count(case when kh.MaKhoaHoc = 'K6' then hs.MaHocTap else 0 end) K6,
			 count(case when kh.MaKhoaHoc = 'K7' then hs.MaHocTap else 0 end) K7,
			 count(case when kh.MaKhoaHoc = 'K7 KCQ' then hs.MaHocTap else 0 end) as 'K7 KCQ',
			 count(case when kh.MaKhoaHoc = 'K7B' then hs.MaHocTap else 0 end) K7B,
			 count(case when kh.MaKhoaHoc = 'K8' then hs.MaHocTap else 0 end) K8,
			 count(case when kh.MaKhoaHoc = 'K9' then hs.MaHocTap else 0 end) K9

from HoSoNhapHoc as hs join KhoaHoc_NganhDaoTao as kn on hs.MaNganh = kn.MaNganh
				       join KhoaHoc as kh on kn.MaKhoaHoc = kh.MaKhoaHoc
					   join NganhDaoTao as n on kn.MaNganh = n.MaNganh
group by n.MaNganh, n.TenNganh


-----------------------------------------------------------------------------------------------
-- 15.Cập nhật lại định mức số sinh viên tối thiểu, tối đa của các lớp học phần được mở trong học kỳ
-- @hocky năm học @namhoc theo định mức số sinh viên tối thiểu, tối  đa của học phần tương ứng.  
-----------------------------------------------------------------------------------------------


-----------------------------------------------------------------------------------------------
-- 16.Cập nhật số giờ được phân công giảng dạy của mỗi giảng viên theo công thức 
-- Số giờ dạy = Số tín chỉ của lớp học phần * 15 / số giáo viên được phân công dạylớp học phần   
-----------------------------------------------------------------------------------------------

Update LopHocPhan_PhanCongDay
set SoGioDay = SoTinChi * 15 / (	select count(MaGV)
			from LopHocPhan_PhanCongDay as lhp_pc
			where MaLopHocPhan = LopHocPhan_PhanCongDay.MaLopHocPhan
			group by MaLopHocPhan
		) 
From HocPhan as hp join LopHocPhan as lhp on hp.MaHocPhan = lhp.MaHocPhan