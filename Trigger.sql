﻿-------------------------------------------------------------------------------
--  trg_nhatkybanhang_insert: Khi có một mặt hàng được thêm vào lịch sử bán hàng
-- Thì tổng số lượng tồn bảng MatHang cũng giảm theo.
-------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'trg_nhatkybanhang_insert')
      DROP TRIGGER dbo.trg_nhatkybanhang_insert;
go

CREATE TRIGGER trg_nhatkybanhang_insert 
ON NhatKyBanHang 
FOR INSERT 
AS 
	SET NOCOUNT ON
	UPDATE mathang 
	SET mathang.soluong = mathang.soluong - inserted.soluong
	FROM mathang INNER JOIN inserted 
	ON mathang.mahang=inserted.mahang

---Test Trigger trg_nhatkybanhang_inser
go

INSERT INTO nhatkybanhang 
 (ngaymua,nguoimua,mahang,soluong,giaban) 
VALUES('5/5/2004','Tran Ngoc Thanh','H1',1,5200) -------------------------------------------------------------------------------
--  trg_nhatkybanhang_update_soluong: Khi cập nhật số lượng cho bảng NhatKyBanHang
--	dựa vào STT thì trigger cũng thay đổi số lượng bảng mặt hàng bằng cách trừ đi
-------------------------------------------------------------------------------IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'trg_nhatkybanhang_update_soluong')
      DROP TRIGGER dbo.trg_nhatkybanhang_update_soluong;
goCREATE TRIGGER trg_nhatkybanhang_update_soluong 
ON NhatKyBanHang 
FOR UPDATE 
AS 
IF UPDATE(soluong) 
	SET NOCOUNT ON
	UPDATE mathang 
	SET mathang.soluong = mathang.soluong - (inserted.soluong - deleted.soluong) 
	FROM (deleted INNER JOIN inserted ON 
	deleted.stt = inserted.stt) INNER JOIN mathang 
	ON mathang.mahang = deleted.mahang--Test Trigger trg_nhatkybanhang_update_soluongUPDATE nhatkybanhang 
SET soluong = soluong + 9
WHERE stt = 18

select * from NhatKyBanHang
-------------------------------------------------------------------------------
--								 ROLLBACK TRANSACTION:
-- trg_nhatkybanhang_insert_with_rollback: Khi có một mặt hàng được thêm vào lịch sử bán hàng

-- thì tổng số lượng tồn bảng MatHang cũng giảm theo.
-- Tuy nhiên phải thoả mãn điều kiện
-- 1. Nếu số lượng hàng hiện có (tồn) nhỏ hơn số lượng bán thì huỷ bỏ thao tác bổ sung dữ liệu 
-- 2. Nếu dữ liệu hợp lệ thì giảm số lượng hàng hiện có 
-------------------------------------------------------------------------------
if exists(select * from sys.triggers where name = 'trg_nhatkybanhang_insert_with_rollback')
	drop trigger dbo.trg_nhatkybanhang_insert_with_rollback
go	
CREATE TRIGGER trg_nhatkybanhang_insert_with_rollback
ON NHATKYBANHANG 
FOR INSERT 
AS 
 DECLARE @sl_ton int /* Số lượng hàng hiện có */ 
 DECLARE @sl_ban int /* Số lượng hàng được bán */ 
 DECLARE @mahang nvarchar(5) /* Mã hàng được bán */ 
 

 SELECT @mahang = mahang, @sl_ban = soluong  
 FROM inserted
 SELECT @sl_ton = soluong 
 FROM mathang where mahang=@mahang 

 IF @sl_ton < @sl_ban 
	ROLLBACK TRANSACTION 
 ELSE 
	 UPDATE mathang 
	 SET soluong = soluong - @sl_ban 
	 WHERE mahang = @mahang


---Test Trigger trg_nhatkybanhang_insert_with_rollback

INSERT INTO nhatkybanhang (ngaymua,nguoimua,mahang,soluong,giaban) 
VALUES('5/5/2004','Tran Ngoc Thanh','H3', 1, 5200) 


INSERT INTO nhatkybanhang 
 (ngaymua,nguoimua,mahang,soluong,giaban) 
VALUES('5/5/2004','Tran Ngoc Thanh','H1',1,5200) 
-------------------------------------------------------------------------------
--  trg_nhatkybanhang_update_soluong_v2: Cập nhật số lượng cho bảng NhatKyBanHang
--	dựa vào STT thì trigger
-------------------------------------------------------------------------------
if exists (select * from sys.triggers where name = 'trg_nhatkybanhang_update_soluong_v2')
	drop trigger trg_nhatkybanhang_update_soluong_v2
go

create trigger trg_nhatkybanhang_update_soluong_v2
on NhatKyBanHang
For update
as
	if(UPDATE(soluong))
		if(@@ROWCOUNT = 1)	
			begin
				Update MatHang
				set MatHang.soluong  = m.soluong - (i.soluong - t.soluong)
				from (inserted as i join deleted as t on i.stt = t.stt) join MatHang as m on i.mahang = m.mahang
			end
		else 
			begin 
			UPDATE mathang 
			 SET mathang.soluong = mathang.soluong - 
			 (SELECT SUM(inserted.soluong-deleted.soluong) 
			 FROM inserted INNER JOIN deleted 
			 ON inserted.stt=deleted.stt 
			 WHERE inserted.mahang = mathang.mahang) 
			 WHERE mathang.mahang IN (SELECT mahang 
			 FROM inserted) 
		end