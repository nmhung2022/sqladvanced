-- Yêu cầu: Thay đổi thiết kế của tất cả các bảng trong CSDL SampleShopDB: Thiết lập tính chất IDENTITY 
-- cho tất cả các cột Id (khóa chính) trong các bảng

-- Hướng dẫn:
--		+ Chọn bảng cần thiết kế, nhấp phải chọn Design
--		+ Trong cửa sổ Design, chọn cột cần thiết lập tính chất
--		+ Trong phần Column Properties -> Identity Specification -> Is Identity: chuyển thành Yes
--		+ Lưu lại thiết kế

-- Lưu ý: Nếu khi lưu mà gặp lỗi không cho lưu thì điều chỉnh lại chế độ của phần mềm:
--		+ Chọn menu Tools -> Options -> Designers
--		+ Bỏ mục chọn "Prevent saving changes that require table re-creation"

-- Câu 1: Viết các thủ tục thực hiện các yêu cầu dưới đây
-- Lưu ý: 
--      + Trong thủ tục phải kiểm soát được tính hợp lệ của dữ liệu đầu vào
--      + Đối với những thủ tục bổ sung dữ liệu phải có tham số đầu ra (kiểu int) theo qui ước
--              * Nếu bổ sung thành công: tham số đầu ra là Id của bản ghi được bổ sung
--              * Nếu bổ sung không thành công: tham số đầu ra bằng 0

-------------------------------------------------------------------------------
--  proc_Categories_Insert  :	Bổ sung một bản ghi cho bảng Categories
-------------------------------------------------------------------------------
if (exists(select * from sys.objects where name = 'proc_Categories_Insert'))
	drop procedure proc_Categories_Insert;
go
create procedure proc_Categories_Insert
	@CategoryId int output,
	@CategoryName nvarchar(255),
	@Description nvarchar(255) = null
as
begin
	set nocount on;
	
	set @CategoryId = 11;
	-- Tên loại hàng không được null và không được rỗng
	if (@CategoryName is null) or (@CategoryName = N'')
		return;
	-- Tên loại hàng không được trùng
	if (exists(select * from Categories where CategoryName = @CategoryName))
		return;

	insert into Categories(CategoryName, Description)
	values (@CategoryName, @Description);

	set @CategoryId = SCOPE_IDENTITY(); -- Có thể dùng @@IDENTITY thay vì dùng SCOPE_IDENTITY()
end
go

-- Test thủ tục: proc_Categories_Insert
declare @CategoryId int;
exec proc_Categories_Insert
		@CategoryId = @CategoryId output,
		@CategoryName = N'Cơm sinh viên new',
		@Description = N'Ngon bổ rẻ new';
select	@CategoryId;
select * from Categories;
go

-------------------------------------------------------------------------------
--  proc_Categories_Update  :   Cập nhật một bản ghi trong bảng Categories
-------------------------------------------------------------------------------
if (exists(select * from sys.objects where name = 'proc_Categories_Update'))
	drop procedure proc_Categories_Update;
go
create procedure proc_Categories_Update
	@CategoryId int,
	@CategoryName nvarchar(255),
	@Description nvarchar(255) = null,
	@Result bit output
as
begin
	set nocount on;

	if (exists(select * from Categories where CategoryName = @CategoryName and CategoryId <> @CategoryId))
		begin
			set @Result = 0;
			return;
		end

	update	Categories
	set		CategoryName = @CategoryName, 
			Description = @Description
	where	CategoryId = @CategoryId;

	if (@@ROWCOUNT > 0)	-- @@ROWCOUNT cho biết số dòng dữ liệu bị tác động bởi câu lệnh vừa thực hiện
		set @Result = 1
	else 
		set @Result = 0	
end
go

-- Test thủ tục:
declare @Result bit;
exec proc_Categories_Update
	@CategoryId = 17,
	@CategoryName = N'Bia sinh viên',
	@Description = N'Uống ít, say nhiều',
	@Result = @Result output;
select @Result;

select * from Categories;
go

-------------------------------------------------------------------------------
--  proc_Categories_Delete	:   Xóa một bản thi khỏi bảng Categories
-------------------------------------------------------------------------------
if (exists(select * from sys.objects where name = 'proc_Categories_Delete'))
	drop procedure proc_Categories_Delete;
go
create procedure proc_Categories_Delete
	@CategoryId int,
	@Result bit output
as
begin
	set nocount on;

	delete from Categories
	where	(CategoryId = @CategoryId)
		and	(not exists(select * from Products where CategoryId = @CategoryId));

	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = 0;
end
go

-- Test thủ tục:
declare @R int;
exec proc_Categories_Delete
	@CategoryId = 1,
	@Result = @R output;
select @R;

select * from Categories;
-------------------------------------------------------------------------------
--  proc_Suppliers_Insert	:   Bổ sung một bản ghi cho bảng Suppliers
-------------------------------------------------------------------------------
if (exists(select * from sys.objects where name = 'proc_Suppliers_Insert'))
	drop procedure proc_Suppliers_Insert;
go
create procedure proc_Suppliers_Insert
	@SupplierId int output,
	@SupplierName nvarchar(255),
	@ContactName nvarchar(255),
	@Address nvarchar(255) = null,
	@City nvarchar(255) = null,
	@PostalCode nvarchar(255) = null,
	@Country nvarchar(255) = null,
	@Phone nvarchar(255) = null
as
begin
	set nocount on;
	
	set @SupplierId = 0;
	-- Tên loại hàng không được null và không được rỗng
	if (@SupplierName is null) or (@SupplierName = N'')
		return;
	if (@ContactName is null) or (@ContactName = N'')
		return;
	-- Tên loại hàng không được trùng
	if (exists(select * from Suppliers where SupplierName = @SupplierName))
		return;

	insert into Suppliers(SupplierName, ContactName, Address, City, PostalCode, Country, Phone)
	values (@SupplierName, @ContactName, @Address, @City, @PostalCode, @Country, @Phone);

	set @SupplierId = SCOPE_IDENTITY(); -- Có thể dùng @@IDENTITY thay vì dùng SCOPE_IDENTITY()
end
go

-- Test thủ tục: proc_Suppliers_Insert
declare @SupplierId int;
exec proc_Suppliers_Insert
		@SupplierId = @SupplierId output,
		@SupplierName = N'Cơm sinh viên new',
		@ContactName = N'Ngon bổ rẻ new',
		@Address = N'Ngon bổ rẻ new',
		@City = N'Ngon bổ rẻ new',
		@Country = N'Ngon bổ rẻ new',
		@Phone = N'Ngon bổ rẻ new';

select	@SupplierId;
select * from Suppliers;
go

-------------------------------------------------------------------------------
--  proc_Suppliers_Update   :   Cập nhật một bản ghi trong bảng Suppliers
-------------------------------------------------------------------------------
if (exists(select * from sys.objects where name = 'proc_Suppliers_Update'))
	drop procedure proc_Categories_Update;
go
create procedure proc_Suppliers_Update
	@SupplierId int output,
	@SupplierName nvarchar(255),
	@ContactName nvarchar(255),
	@Address nvarchar(255) = null,
	@City nvarchar(255) = null,
	@PostalCode nvarchar(255) = null,
	@Country nvarchar(255) = null,
	@Phone nvarchar(255) = null,
	@Result bit output
as
begin
	set nocount on;

	if (exists(select * from Suppliers where SupplierName = @SupplierName and SupplierId <> @SupplierId))
		begin
			set @Result = 0;
			return;
		end

	update	Suppliers
	set		SupplierName = @SupplierName, 
			ContactName = @ContactName, 
			Address = @Address,
			City = @City,
			PostalCode = @PostalCode,
			Country = @Country,
			Phone = @Phone
	where	SupplierId = @SupplierId;

	if (@@ROWCOUNT > 0)	-- @@ROWCOUNT cho biết số dòng dữ liệu bị tác động bởi câu lệnh vừa thực hiện
		set @Result = 1
	else 
		set @Result = 0	
end
go

-- Test thủ tục:
declare @Result bit;
exec proc_Suppliers_Update
	@SupplierId = 0,
	@SupplierName = N'Cơm sinh viên update',
	@ContactName = N'Ngon bổ rẻ update',
	@Address = N'Ngon bổ rẻ update',
	@City = N'Ngon bổ rẻ update',
	@Country = N'Ngon bổ rẻ update',
	@Phone = N'Ngon bổ rẻ update',

	@Result = @Result output;

select @Result;
select * from Suppliers;
go


-------------------------------------------------------------------------------
--  proc_Suppliers_Delete   :   Xóa một bản ghi khỏi bảng Suppliers
-------------------------------------------------------------------------------
if (exists(select * from sys.objects where name = 'proc_Suppliers_Delete'))
	drop procedure proc_Suppliers_Delete;
go
create procedure proc_Suppliers_Delete
	@SupplierId int,
	@Result bit output
as
begin
	set nocount on;

	delete from Suppliers
	where	(SupplierId = @SupplierId)
		and	(not exists(select * from Products where SupplierId = @SupplierId));

	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = 0;
end
go

-- Test thủ tục:
declare @R int;
exec proc_Suppliers_Delete
	@SupplierId = 0,
	@Result = @R output;

select @R;
select * from Categories;


-------------------------------------------------------------------------------
--  proc_Customers_Insert   :   Bổ sung một bản ghi cho bảng Customers
-------------------------------------------------------------------------------
if (exists(select * from sys.objects where name='proc_Customers_Insert'))
	drop procedure proc_Customers_Insert
go

create procedure proc_Customers_Insert
	@CustomerId int output,
	@CustomerName nvarchar(255),
	@ContactName nvarchar(255),
	@Address nvarchar(255) = null,
	@City nvarchar(255) = null,
	@PostalCode nvarchar(255) = null,
	@Country nvarchar(255) = null
as
	begin
		set nocount on;
		set @CustomerId = 0
		
		if(@CustomerName is null) or (@CustomerName = N'')
			return;
		if(@ContactName is null) or (@ContactName = N'')
			return;
		if(exists(select  * from Customers where CustomerName = @CustomerName))
			return;
		insert into Customers(CustomerName, ContactName, Address, City, PostalCode, Country)
		values(@CustomerName, @ContactName, @Address, @City, @PostalCode, @Country)
		set @CustomerId = @@IDENTITY; 
	end
go

-- Test thủ tục: proc_Customers_Insert

declare @CustomerId int;
exec proc_Customers_Insert
		@CustomerId = @CustomerId output,
		@CustomerName = N'Nguyễn Văn A new',
		@ContactName = N'Nguyễn Văn A new ',
		@Address = N'Nguyễn Văn A new',
		@City = N'Nguyễn Văn Anew',
		@Country = N'Nguyễn Văn A new'

select	@CustomerId;
select * from Customers;
go
-------------------------------------------------------------------------------
--  proc_Customers_Update   :   Cập nhật một bản thi trong bảng Customers
-------------------------------------------------------------------------------
if(exists(select * from sys.objects where name = 'proc_Customers_Update'))
	drop procedure proc_Customers_Update
go 

create procedure proc_Customers_Update
	@CustomerId int output,
	@CustomerName nvarchar(255),
	@ContactName nvarchar(255),
	@Address nvarchar(255) = null,
	@City nvarchar(255) = null,
	@PostalCode nvarchar(255) = null,
	@Country nvarchar(255) = null,
	@Result bit output
as
begin
	set nocount on;
	if(exists(select * from Customers where CustomerName = @CustomerName and CustomerId <> @CustomerId))
		begin
			set @Result =0;
			return;
		end
	update Customers
	set	CustomerName = @CustomerName,
		ContactName = @ContactName,
		Address = @Address,
		City = @City,
		PostalCode = @PostalCode,
		Country = @Country
	where CustomerId = @CustomerId

	if(@@ROWCOUNT > 0)
		set @Result = 1;
	else 
		set @Result = 0;
end
go


-- Test thủ tục:
declare @Result bit;
exec proc_Customers_Update
	@CustomerId = 0,
	@CustomerName = N'Nguyễn Văn A new',
	@ContactName = N'Nguyễn Văn A new ',
	@Address = N'Nguyễn Văn A new',
	@City = N'Nguyễn Văn Anew',
	@Country = N'Nguyễn Văn A new',
	@Result = @Result output;

select @Result;
select * from Customers;
go


-------------------------------------------------------------------------------
--  proc_Customers_Delete   :   Xóa một bản ghi khỏi bảng Customers
-------------------------------------------------------------------------------
if(exists(select * from sys.objects where name = 'proc_Customers_Delete'))
	drop procedure proc_Customers_Delete
go
create procedure proc_Customers_Delete
	@CustomerId int,
	@Result bit output
as 
begin
	set nocount on;
	delete from Customers
	where (CustomerId = @CustomerId) 
	and (not exists(select * from Customers where CustomerId = @CustomerId))
	
	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = 0;
end
go
-- Test thủ tục:
declare @R int;
exec proc_Customers_Delete
	@CustomerId = 0,
	@Result = @R output;

select @R;
select * from Categories;



-------------------------------------------------------------------------------
--  proc_Employees_Insert   :   Bổ sung một bản ghi cho bảng Employees
-------------------------------------------------------------------------------
if(exists(select * from sys.objects where name = 'proc_Employees_Insert'))
	drop procedure proc_Employees_Insert
go

create procedure proc_Employees_Insert
	@EmployeeId int output,
	@LastName nvarchar(255),
	@FirstName nvarchar(255),
	@BirthDate date= null,
	@Photo nvarchar(255) = null,
	@Notes ntext = null

as
begin
	set nocount on;
	set @EmployeeId = 0

	if(@LastName is null) or (@LastName = N'')
		return;
	if(@FirstName is null) or (@FirstName = N'')
		return
	insert into Employees (LastName, FirstName, BirthDate, Photo, Notes)
	values (@LastName, @FirstName, @BirthDate, @Photo, @Notes)
	set @EmployeeId = @@IDENTITY; 
end
go

declare @EmployeeId int;
exec proc_Employees_Insert
		@EmployeeId = @EmployeeId output,
		@LastName = N'Nhân viên A',
		@FirstName = 'new',
		@BirthDate = '1900-08-08'
select	@EmployeeId;
select * from Employees;
go
	



-------------------------------------------------------------------------------
--  proc_Employees_Update   :   Cập nhật một bản ghi trong bảng Employees
-------------------------------------------------------------------------------
if(exists(select * from sys.objects where name = 'proc_Employees_Update'))
	drop procedure proc_Employees_Update
go 

create procedure proc_Employees_Update
	@EmployeeId int output,
	@LastName nvarchar(255),
	@FirstName nvarchar(255),
	@BirthDate date= null,
	@Photo nvarchar(255) = null,
	@Notes ntext = null,
	@Result bit output
as
begin
	set nocount on;

	update Employees
	set	LastName = @LastName,
		FirstName = @FirstName,
		BirthDate = @BirthDate,
		Photo = @Photo,
		Notes = @Notes		
	where EmployeeId = @EmployeeId

	if(@@ROWCOUNT > 0)
		set @Result = 1;
	else 
		set @Result = 0;
end
go


-- Test thủ tục:
declare @Result bit;
exec proc_Employees_Update
	@EmployeeId = 12,
	@LastName = N'Nhân viên A',
	@FirstName = 'update',
	@BirthDate = '1900-08-08',
	@Result = @Result output;

select @Result;
select * from Employees;
go



-------------------------------------------------------------------------------
--  proc_Employees_Delete   :   Xóa một bản khi khỏi bảng Employees
-------------------------------------------------------------------------------
if(exists(select * from sys.objects where name = 'proc_Employees_Delete'))
	drop procedure proc_Employees_Delete

go
create procedure proc_Employees_Delete
	@EmployeeId int,
	@Result bit output
as 
begin
	set nocount on;

	delete from Employees
	where (EmployeeId = @EmployeeId) 
		and (exists(select * from Employees where EmployeeId = @EmployeeId))
	
	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = 0;
end
go
-- Test thủ tục:
declare @R int;
exec proc_Employees_Delete
	@EmployeeId = 13,
	@Result = @R output;
select @R;

select * from Employees;




-------------------------------------------------------------------------------
--  proc_Shippers_Insert    :   Bố sung một bản ghi cho bảng Shippers
-------------------------------------------------------------------------------
if(exists(select * from sys.objects where name = 'proc_Shippers_Insert'))
	drop procedure proc_Shippers_Insert
go

create procedure proc_Shippers_Insert
	@ShipperId int output,
	@ShipperName nvarchar(255),
	@Phone nvarchar(255) = null
as
begin
	set nocount on;
	set @ShipperId = 0

	if(@ShipperName is null) 
		return;
	insert into Shippers(ShipperName, Phone)
	values (@ShipperName, @Phone)
	set @ShipperId = @@IDENTITY; 
end
go

declare @ShipperId int;
exec proc_Shippers_Insert
		@ShipperId = @ShipperId output,
		@ShipperName = N'Shipper new'
		
select	@ShipperId;
select * from Shippers;
go
	


-------------------------------------------------------------------------------
--  proc_Shippers_Update    :   Cập nhật một bản ghi cho bảng Shippers
-------------------------------------------------------------------------------
if(exists(select * from sys.objects where name = 'proc_Shippers_Update'))
	drop procedure proc_Shippers_Update
go 

create procedure proc_Shippers_Update
	@ShipperId int output,
	@ShipperName nvarchar(255),
	@Phone nvarchar(255) = null,
	@Result bit output
as
begin
	set nocount on;

	update Shippers
	set	ShipperName = @ShipperName,
		Phone = @Phone
	where ShipperId = @ShipperId

	if(@@ROWCOUNT > 0)
		set @Result = 1;
	else 
		set @Result = 0;
end
go


-- Test thủ tục:
declare @Result bit;
exec proc_Shippers_Update
	@ShipperId = 12,
	@ShipperName = N'Shipper update',
	@Phone = '(084) 333-4441',
	@Result = @Result output;

select @Result;
select * from Shippers;
go


-------------------------------------------------------------------------------
--  proc_Shippers_Delete    :   Xóa một bản ghi khỏi bảng Shippers
-------------------------------------------------------------------------------
if(exists(select * from sys.objects where name = 'proc_Shippers_Delete'))
	drop procedure proc_Shippers_Delete

go
create procedure proc_Shippers_Delete
	@ShipperId int,
	@Result bit output
as 
begin
	set nocount on;

	delete from Shippers
	where (ShipperId = @ShipperId) 
		and (exists(select * from Shippers where ShipperId = @ShipperId))
	
	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = 0;
end
go
-- Test thủ tục:
declare @R int;
exec proc_Shippers_Delete
	@ShipperId = 7,
	@Result = @R output;
select @R;

select * from Shippers;




-- Câu 2: Viết thủ tục
--    proc_Orders_Get
--        @OrderId int,
--        @SumOfQuantity int output,
--        @SumOfMoney money output
-- Có chức năng hiển thị danh sách các mặt hàng, số lượng, đơn giá và số tiền của các mặt hàng được bán trong đơn hàng có mã là @OrderId. 
-- Đồng thời cho biết được tổng số lượng hàng được bán và tổng số tiền hàng thông qua 2 tham số đầu ra là @SumOfQuantity và @SumOfMoney. 

if(exists(select * from sys.objects where name = 'proc_Orders_Get'))
	drop procedure proc_Orders_Get
go

create procedure proc_Orders_Get
	@OrderId int,
	@SumOfQuantity int output,
	@SumOfMoney money output

as 
begin
	set nocount on;
	if(@OrderId is null) 
		return;
	select c.CategoryName, od.Quantity, od.SalePrice
	from OrderDetails as od Join Products as p on p.ProductId = od.ProductId Join Categories as c On c.CategoryId = p.CategoryId
	where od.OrderId = @OrderId

	select @SumOfMoney = SUM(od.SalePrice * od.Quantity), @SumOfQuantity = Sum(od.Quantity)
	from OrderDetails as od Join Products as p on p.ProductId = od.ProductId Join Categories as c On c.CategoryId = p.CategoryId
	where od.OrderId = @OrderId

end
go

declare @R1 int;
declare @R2 money;
exec proc_Orders_Get
	@OrderId = 10248,
	@SumOfQuantity = @R1 output,
	@SumOfMoney = @R2 output;

select @R1 As SumOfQuantity , @R2 As SumOfMoney


--Câu 3: Viết thủ tục
-- 	proc_SummaryRevenueByMonth
--		@Year int
--      @SumOfQuantity int output
--      @SumOfRevenue money output
-- Có chức năng thống kê tổng số lượng hàng và tổng doanh thu bán hàng trong từng tháng của năm @Year. 
-- Kết quả thống kê phải hiển thị đủ 12 tháng của năm, kể cả những tháng không bán được hàng.
-- Đồng thời cho biết được tổng số lượng hàng được bán và tổng doanh thu của năm @Year thông qua 2 tham số đầu ra là @SumOfQuantity và @SumOfRevenue
if(exists(select * from sys.objects where name = 'proc_SummaryRevenueByMonth'))
	drop procedure proc_SummaryRevenueByMonth
go

create procedure proc_SummaryRevenueByMonth
	@Year int,
	@SumOfQuantity int output,
	@SumOfRevenue money output

as 
begin
	set nocount on;
	if(@Year is null) 
		return;

	declare @tblSummary table
	(
		Month int primary key,
		Revenue money default(0),
		Quantity int default(0)
		
	);
	insert into @tblSummary

		select	month(o.OrderDate) as Month, sum(od.Quantity) As Quantity, sum(od.Quantity * od.SalePrice) as Revenue
		from	Orders as o 
				join OrderDetails as od on o.OrderId = od.OrderId
		where	year(o.OrderDate) = @Year 
		group by month(o.OrderDate);

	declare @month int = 1;
	
	while (@month <= 12)
		begin
			if (not exists(select * from @tblSummary where Month = @month))
				insert into @tblSummary(Month) values(@month);
			set @month = @month + 1;
		end
	select * from @tblSummary Order by MONTH
	select @SumOfQuantity = sum(Quantity), @SumOfRevenue = sum(Revenue) from @tblSummary 
	
end
go

declare @R1 int;
declare @R2 money;

exec proc_SummaryRevenueByMonth
	@Year = 2017,
	@SumOfQuantity = @R1 output,
	@SumOfRevenue = @R2 output;

select @R1 As SumOfQuantity , @R2 As SumOfMoney





--Câu 4: Viết thủ tục:
-- 	proc_SummaryRevenueByDate
--		@FromDate date,
--		@ToDate date,
--      @SumOfQuantity int output
--      @SumOfRevenue money output
-- Có chức năng thống kê tổng số lượng hàng và tổng doanh thu bán hàng của từng ngày trong khoảng thời gian từ ngày @FromDate đến ngày @ToDate. 
-- Kết quả thống kê phải hiển thị đủ các ngày trong khoảng thời gian trên, kể cả những ngày không bán được hàng.
-- Đồng thời cho biết được tổng số lượng hàng được bán và tổng doanh thu từ ngày @FromDate đến ngày @ToDate thông qua 2 tham số đầu ra là @SumOfQuantity và @SumOfRevenue

if(exists(select * from sys.objects where name = 'proc_SummaryRevenueByDate'))
	drop procedure proc_SummaryRevenueByDate
go

create procedure proc_SummaryRevenueByDate
	@FromDate date,
	@ToDate date,
	@SumOfQuantity int output,
	@SumOfRevenue money output

as 
begin
	set nocount on;
	if(@FromDate is null) and (@ToDate is null) 
		return;

	declare @tblSummary table
	(
		Day int primary key,
		Revenue money default(0),
		Quantity int default(0)
		
	);
	insert into @tblSummary

		select	Day(o.OrderDate) as Day, sum(od.Quantity) As Quantity, sum(od.Quantity * od.SalePrice) as Revenue
		from	Orders as o 
				join OrderDetails as od on o.OrderId = od.OrderId
		where	o.OrderDate between @FromDate and @ToDate
		group by Day(o.OrderDate);

	declare @day int = 1;
	
	while ((Day(@FromDate) <= @day) and (@day <= Day(@ToDate)))
		begin
			if (not exists(select * from @tblSummary where Day = @day))
				insert into @tblSummary(Day) values(@day);
			set @day = @day + 1;
		end

	select * from @tblSummary Order by Day
	select @SumOfQuantity = sum(Quantity), @SumOfRevenue = sum(Revenue) from @tblSummary 
	
end
go

declare @R1 int;
declare @R2 money;

exec proc_SummaryRevenueByDate

	@FromDate = '2014-01-01',
	@ToDate = '2019-01-01',
	@SumOfQuantity = @R1 output,
	@SumOfRevenue = @R2 output;
	

select @R1 As SumOfQuantity , @R2 As SumOfMoney

go


-----------------------------------------------------------------------------------------
--Câu 5: Viết thủ tục (Truy vấn phân trang/pagination)
-- 	proc_Customers_Select
-- 		@Page int = 1,
--		@PageSize int = 20,
-- 		@FindValue nvarchar(255) = N'',
--      @RowCount int output
--Có chức năng tìm kiếm và hiển thị thông tin về các khách hàng, với yêu cầu sau:
--    + Tìm kiếm tương đối theo CustomerName hoặc ContactName dựa vào giá trị của @FindValue (Nếu @FindValue là chuỗi rỗng thì 
--		không tìm kiếm)
--    + Dữ liệu hiển thị được phân trang với số dòng trên mỗi trang là @PageSize, tham số @Page cho biết trang cần hiển thị.
--    + Tham số đầu ra @RowCount cho biết tổng số dòng dữ liệu truy vấn được
-----------------------------------------------------------------------------------------
if (exists(select * from sys.objects where name = 'proc_Customers_Select'))
	drop procedure proc_Customers_Select;
go
create procedure proc_Customers_Select
	@Page int = 1,
	@PageSize int = 20,
	@FindValue nvarchar(255) = N'',
	@RowCount int output
as
begin
	set nocount on;

	if (@FindValue <> N'')
		set @FindValue = '%' + @FindValue + '%';
	
	-- Truy vấn dữ liệu đưa vào 1 bảng tạm
	select	*,
			row_number() over(order by CustomerName) as RowNumber
	into	#TempCustomers
	from	Customers
	where	(@FindValue = N'')
		or	((CustomerName like @FindValue) or (ContactName like @FindValue));
	
	-- Xác định tổng số dòng dữ liệu truy vấn được 
	set @RowCount = @@ROWCOUNT;

	-- Lấy dữ liệu để hiển thị: dữ liệu tại trang thứ @Page

	select	*
	from	#TempCustomers
	where	RowNumber between (@Page - 1) * @PageSize + 1 and @Page * @PageSize;
end
go

-- Test thủ tục:
declare @RowCount int;
exec proc_Customers_Select
	@Page = 1,
	@PageSize = 5,
	@FindValue = N'',
	@RowCount = @RowCount output;
select @RowCount;
go

--Câu 6: Viết thủ tục:
-- 	proc_Products_Select
-- 		@Page int = 1,
--		@PageSize int = 20,
--		@ProductName nvarchar(255) = N'',
--        @RowCount int output
--Có chức năng tìm kiếm và hiển thị thông tin về các mặt hàng, nhà cung cấp, phân loại của các mặt hàng, với yêu cầu sau:
--    + Tìm kiếm tương đối theo tên hàng dựa vào giá trị của tham số @ProductName (Nếu @ProductName là chuỗi rỗng thì không tìm kiếm);
--    + Dữ liệu hiển thị được phân trang với số dòng trên mỗi trang là @PageSize, tham số @Page cho biết trang cần hiển thị 
--      (Trường hợp @PageSize bằng 0 thì hiển thị toàn bộ dữ liệu tìm được).
--    + Tham số đầu ra @RowCount cho biết tổng số dòng dữ liệu truy vấn được
if (exists(select * from sys.objects where name = 'proc_Products_Select'))
	drop procedure proc_Products_Select;
go
create procedure proc_Products_Select
	@Page int = 1,
	@PageSize int = 20,
	@ProductName nvarchar(255) = N'',
	@RowCount int output
as
begin
	set nocount on;

	if (@ProductName <> N'')
		set @ProductName = '%' + @ProductName + '%';
	
	-- Truy vấn dữ liệu đưa vào 1 bảng tạm
	select	*,
			row_number() over(order by ProductName) as RowNumber
	into	#TempProducts
	from	Products
	where	(@ProductName = N'') or	((ProductName like @ProductName))
	
	-- Xác định tổng số dòng dữ liệu truy vấn được 
	set @RowCount = @@ROWCOUNT;

	-- Lấy dữ liệu để hiển thị: dữ liệu tại trang thứ @Page
	if(@PageSize = 0)
		select	*
		from	#TempProducts
	else
		select	*
		from	#TempProducts
		where	RowNumber between (@Page - 1) * @PageSize + 1 and @Page * @PageSize;
end
go

-- Test thủ tục:
declare @RowCount int;
exec proc_Products_Select
	@Page = 1,
	@PageSize = 5,
	@ProductName = N'',
	@RowCount = @RowCount output;
select @RowCount;
go




--Câu 7: Định nghĩa kiểu dữ liệu dạng bảng có tên là TypeOrderDetails với các trường như sau:
-- 		ProductId int primary key 
--		Quantity int
--		SalePrice money
--Sử dụng kiểu dữ liệu trên, viết thủ tục sau đây:
-- 	proc_Order_Create
--	 	@CustomerId int,
--	 	@EmployeeId int,
--	 	@OrderDate date,
--	 	@ShipperId int,
--	 	@OrderDetails TypeOrderDetails readonly,
--      @OrderId int output
--Có chức năng tạo mới một đơn hàng và chi tiết của đơn hàng (bổ sung dữ liệu cho bảng Orders và OrderDetails). 
--Lưu ý kiểm tra tính đúng đắn của dữ liệu khi bổ sung.  


--Câu 8: Viết thủ tục:
-- 	proc_Orders_UpdateDiscount
-- 		@OrderId int,
--		@Discount float,
-- 		@SumOfPrice money output,
-- 		@SumOfPriceAfterDiscount money output
--Có chức năng cập nhật lại mức giảm giá là @Discount cho đơn hàng có mã là @OrderId, 
--sau đó tính tổng giá trị của đơn hàng và tổng giá trị sau khi đã được giảm giá 
--(Kết quả tính toán lưu trong các tham số đầu ra @SumOfPrice và @SumOfPriceAfterDiscount).








