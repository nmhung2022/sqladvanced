﻿--Câu 1:  
--a. (0,5 đ) Tạo khung nhìn view_Project_2020 có chức năng lấy thông tin các dự án  bắt đầu trong năm 2020 và thời điểm kết thúc bằng NULL. 
--b. (0,5 đ) Thông qua khung nhìn trên, hãy cập nhật thời điểm kết thúc của các dự án bắt  đầu trong năm 2020 và thời điểm kết thúc bằng NULL thành 31/12/2020. Câu 2: 
--a. (1,5 đ) Viết thủ tục  

-------------------------------------------------------------------------------
-- proc_AddProject: có chức năng bổ sung thêm một dự án mới vào bảng Project. 
--	Có chức năng bổ sung thêm một dự án mới vào bảng Project. 
--  Thủ tục phải kiểm tra được tính hợp lệ của dữ liệu đầu vào, 
--	trong đó nếu như dự án đã có thời điểm kết thúc thì thời 
--	điểm kết thúc phải sau thời điểm bắt đầu của dự án
-------------------------------------------------------------------------------
if exists(select * from sys.objects where name = 'proc_AddProject')
	drop procedure proc_AddProject
go
create procedure proc_AddProject
@ProjectID int, 
@ProjectName nvarchar(50), 
@StartDate date, 
@EndDate date = NULL 
as
begin
	set nocount on
end
go
--Test thủ tục
exec proc_AddProject
@ProjectID = 1, 
@ProjectName = '', 
@StartDate = '', 
@EndDate  = '' 


--b. (1 đ) Viết thủ tục 
-------------------------------------------------------------------------------
-- proc_UpdateProjectEndDate: Có chức năng cập nhật thời điểm kết thúc của dự án có mã là @ProjectID. 
-- Lưu ý chỉ được cập nhật nếu như thời điểm kết thúc phải sau thời điểm bắt đầu của dự án.
-------------------------------------------------------------------------------
if exists(select * from sys.objects where name = 'proc_UpdateProjectEndDate')
	drop procedure proc_UpdateProjectEndDate
go
create procedure proc_UpdateProjectEndDate 
@ProjectID int, 
@EndDate date 
as
begin
	set nocount on
end
go
--Test thủ tục
exec proc_UpdateProjectEndDate
@ProjectID = 1, 
@EndDate  = ''


-- d. 
-------------------------------------------------------------------------------
-- proc_Employees_ListPagination: có chức năng hiển thị danh sách nhân viên dưới dạng phân trang dữ liệu. 
--	Trong đó,  @Page là trang cần hiển thị và @PageSize là số dòng dữ liệu được hiển thị trên mỗi  trang. 
--	Tham số đầu ra @RowCount cho biết tổng số lượng nhân viên hiện có. 
-------------------------------------------------------------------------------
--

if exists(select * from sys.objects where name = 'proc_Employees_ListPagination')
	drop procedure proc_Employees_ListPagination
go
create procedure proc_Employees_ListPagination 
@Page int, 
@PageSize int, 
@RowCount int output 
as
begin
	set nocount on
end
go
--Test thủ tục
declare @RowCount int
exec proc_Employees_ListPagination
@Page = 2, 
@PageSize = 2, 
@RowCount = @RowCount output 

select @RowCount


--proc_CountProjectsByMonth @Year int 
--có chức năng thống kê số lượng dự án bắt đầu trong mỗi tháng của năm @Year. Yêu cầu  kết quả thống kê phải hiển thị đủ 12 tháng của năm. 
--Câu 3: (1đ) Viết trigger tg_Assignment_Insert để xử lý trường hợp bổ sung  dữ liệu vào bảng Assignment theo yêu cầu: Nếu thời điểm giao việc của dự án cho nhân  viên nằm sau thời điểm kết thúc của dự án thì không cho phép bổ sung dữ liệu. Câu 4: 
--a. (1đ) Viết hàm func_CountAssignments(@ProjectID int) có chức năng trả về giá trị cho biết số lượng nhân viên được phân công thực hiện dự án có mã là  @ProjectID. 
--b. (1,5 đ)Viết hàm func_SelectAssignments(@ProjectID int) có chức năng trả về bảng cho biết họ tên, ngày sinh, email, điện thoại, thời điểm giao việc và vai trò của  các nhân viên được phân công thực hiện dự án có mã là @ProjectID.
