﻿--Câu 1 viết thủ tục proc_AuthorizeAccount
-- Kiểm tra đăng nhập
-- Trả về dữ liệu nếu đúng
-- Sai không trả về gì cả
-- 

if exists(select * from sys.objects where name = 'proc_AuthorizeAccount')
	drop proc proc_AuthorizeAccount
go
create procedure proc_AuthorizeAccount
	@Email nvarchar(50),
	@Password nvarchar(50)

as 
begin
	set nocount on
	select top 1 * from Account where Email = @Email and Password = @Password and IsAvailable = 1
end
go
--Tes thủ tục

--Câu 2
-- Gửi tin nhắn cho các accountId

if not exists(select * from sys.types where name = 'TypeListOfIds')
	begin	
		create type TypeListOfIds as Table 
		(
			Id int primary key
		)
	end
go

if exists(select * from sys.objects where name = 'proc_SendMessageToAccounts')
	drop proc proc_SendMessageToAccounts
go
create procedure proc_SendMessageToAccounts
	@AcountId int,
	@Title nvarchar(255),
	@Content nvarchar(1000),
	@Receivers TypeListOfIds readonly,
	@MessageId int output
as
begin
	set nocount on

	if not exists(select * from Account where AccountId = @AcountId and IsAvailable = 1)
		begin
			set @MessageId = 0
			return;
		end

	if(@Title = N'') or (@Content = N'')
		begin
			set @MessageId = -1
			return
		end
	if not exists(select * from @Receivers where Id in (select AccountId from Account))
		begin
			set @MessageId = -2
			return;
		end
	
	insert into Message(MessageTitle, MessageContent, AccountId) 
		values(@Title, @Content, @AcountId)

	set @MessageId = SCOPE_IDENTITY()

	insert into MessageToAccounts(MessageId, AccountId)
	select @MessageId, Id from @Receivers where Id in (select AccountId from Account)
end
go

--Test procedure
declare @Receivers TypeListOfIds;
declare @MessageId int;

insert into @Receivers values (2), (4), (5), (8), (9)
exec proc_SendMessageToAccounts
	@AcountId = 63,
	@Title = 'Haha',
	@Content = N'Hihi, Haha, Hìhì, Hehe',
	@Receivers = @Receivers,
	@MessageId = @MessageId output

if(@MessageId <= 0)
	select @MessageId as ErrorCode
else
	begin
		select * from Message where MessageId = @MessageId
		select * from MessageToAccounts where MessageId = @MessageId
	end



--Câu 3
-- Có chức năng hiện thị danh sách tin nhắn đã được gửi cho tài khoản @accountID
-- Dữ liệu hiện thị dưới dạng phân trang, sắp xếp giảm dần theo thời điểm gửi tin nhắn

if exists(select * from sys.objects where name = 'proc_ListReceivedMessages')
	drop proc proc_ListReceivedMessages
go

create procedure proc_ListReceivedMessages
	@AccountId int,
	@Page int = 1,
	@PageSize int = 20,
	@RowCount int output
as 
begin 
	set nocount on

	if(@Page < 1) set @Page = 1
	if(@PageSize < 1) set @PageSize = 1

	select	m.*, ma.ReadTime, row_number() over(order by m.CreatedTime desc) as RowNumber
	into	#TempMessage
	from	Message as m 
			join MessageToAccounts as ma on m.MessageId = ma.MessageId
	where	ma.AccountId = @AccountId 

	set @RowCount = @@ROWCOUNT;


	select	*
	from 	#TempMessage as t
	where	t.RowNumber between (@Page - 1) * @PageSize + 1 and @Page * @PageSize
	order by t.RowNumber
end
go

--Test thủ tục
declare @RowCount int
exec proc_ListReceivedMessages
	@AccountId = 2,
	@Page = 0,
	@PageSize = 5,
	@RowCount = @RowCount output

select @RowCount as CountOfMessages


--Câu 4
-- Viết trigger, bất kì trường hợp update cột ReadTime trong bảng MessageToAccounts sao cho khi giá trị của cột 
-- ReadTime được thay đổi từ NULL sang giá trị khác null thì tăng giá trị của cột CountOfReads trong bảng Message lên 1
-- Chỉ update 1 dòng trong bảng MessageToAccounts
if exists(select * from sys.triggers where name = 'trg_MessageToAccounts_Update_ReadTime')
	drop trigger trg_MessageToAccounts_Update_ReadTime
go

create trigger trg_MessageToAccounts_Update_ReadTime
on MessageToAccounts for update
as 
begin
	set nocount on
	if UPDATE(ReadTime)
		begin
			declare @MessageId int, @DeletedReadTime datetime, @InsertedReadTime datetime
			
			select @MessageId = MessageId, @DeletedReadTime = ReadTime  from deleted
			select @InsertedReadTime = ReadTime from inserted

			if(@DeletedReadTime is null) and (@InsertedReadTime is not null)
				begin
					update Message
					set CountOfReads += 1
					where MessageId = @MessageId
				end
		end
end
go

--Test trigger
update	MessageToAccounts set ReadTime = GETDATE() where MessageId = 3 and AccountId = 2
update	MessageToAccounts set ReadTime = GETDATE() where MessageId = 3 and AccountId = 4
update	MessageToAccounts set ReadTime = GETDATE() where MessageId = 3 and AccountId = 5
select * from Message where MessageId = 3


-- Câu 5
-- Có chức năng đọc một tin nhắn có mã là @MessageId được gửi đến tài khoản @AccountId
-- Trong trường hợp nếu tin nhắn chưa được đọc (ReadTime là null)
-- Thì cập nhật giá trị trường ReadTime thành thời điểm hiện tại.

if(exists(select * from sys.objects where name = 'proc_ReadMessage'))
	drop procedure proc_ReadMessage
go
create procedure proc_ReadMessage
	@MessageId int,
	@AccountId int
as
begin 
	set nocount on

	update MessageToAccounts
	set ReadTime = GETDATE()
	where MessageId = @MessageId and AccountId = @AccountId and ReadTime is null

	select m.*, ma.ReadTime
	from Message as m join MessageToAccounts as ma on m.MessageId = ma.MessageId
	where ma.MessageId = @MessageId and ma.AccountId = @AccountId
end
go


--Tes thủ tục
exec proc_ReadMessage
	@MessageId = 3,
	@AccountId  = 9

	
--Câu 6
--Trả về một mảng cho biết tổng số tin nhắn và tổng sô tin nhắn đã đọc của mỗi tài khoản (Tất cả tài khoản)

if(exists(select * from sys.objects where name = 'func_GetReceivedMessageSummary'))
	drop function func_GetReceivedMessageSummary
go
create function func_GetReceivedMessageSummary()
returns table
as
return 
(
	select a.AccountId, a.AccountName, a.Email, 
		isnull(m.CountOfReceived, 0)  as CountOfReceived,
		isnull(m.CountOfReads, 0) as CountOfReads

	from Account as a left join
	(
		select m.AccountId, count(*) as CountOfReceived, count(m.ReadTime) as CountOfReads
		from MessageToAccounts as m
		group by m.AccountId
	) as m on a.AccountId = m.AccountId

)
go
		

select * from func_GetReceivedMessageSummary()

