﻿LẬP TRÌNH TRONG SQL SERVER VỚI T-SQL (TRANSACT-SQL)

Nội dung:
	- Các khái niệm cơ bản trong lập trình T-SQL
	- Giao tác SQL
	- Thủ tục lưu trữ (stored procedure)
	- Hàm (user-defined function)
	- Trigger

-----------------------------------------------------------
I. CÁC KHÁI NIỆM CƠ BẢN TRONG LẬP TRÌNH T-SQL
-----------------------------------------------------------
Lập trình với T-SQL: xây dựng tập các câu lệnh được tổ chức tuần tự nhằm mục đích xử lý dữ liệu

1. Khối lệnh
	Trong T-SQL, một khối lệnh được hiểu là một tập bao gồm nhiều câu lệnh và được đặt
	trong cặp từ khóa BEGIN ... END (Tương tự trong C, dùng cặp {....})

Ví dụ:

	declare @x int;
	set @x = 10;
	if (@x <= 100)
		begin
			set @x = @x * 2;
			print @x;
		end
	go

Lưu ý: Lệnh GO dùng để ngắt đoạn đoạn mã lệnh.

2. Biến và khai báo biến

Trong T-SQL, để khai báo biến ta sử dụng cú pháp:

	DECLARE	@tên_biến_1	kiểu_dữ_liệu [= giá_trị_khởi_tạo],
			@tên_biến_1 kiểu_dữ_liệu [= giá_trị_khởi_tạo],
			....
			@tên_biến_n kiểu_dữ_liệu [= giá_trị_khởi_tạo]

Lưu ý:
	- Tên biến phải bắt đầu bởi 1 ký tự @
	- Nếu khai báo một biến mà chưa gán trị thì biến nhận giá trị NULL

Ví dụ:
	
	declare	@hoten nvarchar(255) = N'Trần Nguyên Phong',
			@tuoi int,
			@email nvarchar(255);

	select	@hoten as HoTen, @tuoi as Tuoi, @email as Email;
	go

	select @hoten, @tuoi;	-- Sẽ gặp lỗi vì phía trên đã có lệnh GO

3. Phép gán

Có 2 cách để thực hiện phép gán:

- Cách 1: Sử dụng lệnh SET theo cú pháp:

	SET @tên_biến = biểu_thức

  Mỗi lệnh SET chỉ gán trị cho một biến

  Ví dụ:

		declare @hoten nvarchar(50),
				@tuoi int;

		set @hoten = N'Nguyễn Văn A';
		set @tuoi = 25;

		print @hoten;
		print @tuoi;
		go

- Cách 2: Sử dụng lệnh SELECT để gán trị, theo cú pháp
	
		SELECT	@tên_biến_1 = biểu_thức_1,
				@tên_biến_2 = biểu_thức_2,
				....
		[FROM ... WHERE ... GROUP BY... HAVING....]

	Ví dụ:

		declare @tenhang nvarchar(255),
				@gia money;

		select	@tenhang = ProductName,
				@gia = Price 
		from	Products
		where	ProductId = 20;

		print @tenhang;
		print @gia;
		go

Cách 2 thường được sử dụng trong trường hợp chúng ta truy vấn dữ liệu từ CSDL và đưa kết quả
truy vấn được vào lưu trữ trong các biến.
Khi sử dụng phép gán theo cách này, câu lệnh SELECT chỉ nên trả về tối đa 1 dòng (tức là mỗi một
biến chỉ được gán với 1 giá trị)

Ví dụ:
	
	declare	@tenkhachhang nvarchar(255),
			@diachi nvarchar(255);

	-- Không nên viết phép gán theo cách này:
	select	@tenkhachhang = CustomerName, 
			@diachi = Address
	from	Customers			

	print @tenkhachhang;
	print @diachi;
	go

4. Các cấu trúc điều khiển
Có 2 loại: Rẽ nhánh và lặp

4.1. Rẽ nhánh: Lệnh IF
Cú pháp:
		IF điều_kiện
			Khối_lệnh_của_if
		ELSE
			Khối_lệnh_của_else

Lưu ý: Sau IF và ELSE là một khối lệnh, cho nên nếu có từ 2 lệnh trở lên thì 
phải đặt trong BEGIN ... END

4.2 Lặp: Lệnh WHILE
Cú pháp:
		WHILE điều_kiện				-- Khi điều kiện đang còn đúng thì lặp
			Khối_lệnh_của_while

Chú ý: Trong vòng lặp WHILE có thể sử dụng các lệnh:
	- CONTINUE		:  Lặp lại
	- BREAK			:  ngắt vòng lặp

	Ví dụ: In ra màn hình các số từ 1 cho đến @N

	declare @N int = 100,
			@i int = 1;

	while (@i <= @N)
		begin
			print @i;
			set @i = @i + 1;
		end
	go

5. Biến kiểu bảng
	Để khai báo 1 biến kiểu bảng, sử dụng câu lệnh DECLARE với cú pháp:

	DECLARE @tên_biến TABLE
	(
		@Tên_cột_1	Kiểu_dữ_liệu	Tính_chất,
		@Tên_cột_2	Kiểu_dữ_liêu	Tính_chất,
		....
		@Tên_cột_N	Kiểu_dữ_liệu	Tính_chất
	)

Lưu ý:
	- Khác với bảng vật lý, biến bảng chỉ tồn tại tạm thời trong khối lệnh mà nó được khai báo
	  và thực thi.
	- Với biến kiểu bảng, chúng ta không thể sử dụng các phép xử lý như biến thông thường (phép gán, 
	  sử dụng giá trị để tính toán, các phép toán số học,...). Chúng ta chỉ sử dụng các lệnh
	  hợp lệ trên bảng như SELECT, INSERT, UPDATE, DELETE

Ví dụ:
	declare	@tblSinhVien table
	(
		MaSV nvarchar(50) primary key,
		HoTen nvarchar(255) not null
	)

	insert into @tblSinhVien 
	values (N'SV1', 'Phong'), (N'SV2', 'Trung');

	select * from @tblSinhVien;
									
Trong trường hợp dữ liệu kiểu bảng cần phải sử dụng nhiều lần, thay vì phải khai báo cấu trúc của 
bảng mỗi khi sử dụng, chúng ta có thể tạo ra kiểu dữ liệu dạng bảng và sử dụng khi cần.

Ví dụ:
	-- Tạo kiểu dữ liệu dạng bảng có tên là TableSinhVien
	create type TableSinhVien as table
	(
		MaSV nvarchar(50) primary key,
		HoTen nvarchar(255) not null,
		Tuoi int null
	)
	go
	
	-- Sử dụng kiểu dữ liệu dạng bảng để khai báo biến
	declare @tblSV TableSinhVien;
	insert into @tblSV(MaSV, HoTen) values (N'SV01', 'Phong');
	select * from @tblSV;
	go

Bài tập 1: Xét đoạn lệnh sau đây cho biết doanh thu từng tháng trong năm @year (2017)

	declare @year int = 2017;

	select	month(o.OrderDate) as Month,
			sum(od.Quantity * od.SalePrice) as Revenue
	from	Orders as o 
			join OrderDetails as od on o.OrderId = od.OrderId
	where	year(o.OrderDate) = @year 
	group by month(o.OrderDate);

	go
	
Yêu cầu lập trình để kết quả báo cáo phải có đủ 12 tháng

Cách 1: 
- Dùng 1 biến bảng chỉ có 1 cột Month (tháng) và bổ sung vào biến này đủ 12 tháng
- Lấy biến bảng vừa tạo nối ngoài trái với câu truy vấn ở trên

	declare @year int = 2017;
	declare @tblMonth table
	(
		Month int primary key
	);
	declare @month int = 1;
	while (@month <= 12)
		begin
			insert into @tblMonth values (@month);
			set @month = @month + 1;
		end

	select	t1.Month,
			isnull(t2.Revenue, 0) as Revenue
	from	@tblMonth as t1
			left join
			(
				select	month(o.OrderDate) as Month,
						sum(od.Quantity * od.SalePrice) as Revenue
				from	Orders as o 
						join OrderDetails as od on o.OrderId = od.OrderId
				where	year(o.OrderDate) = @year 
				group by month(o.OrderDate)			
			) as t2 on t1.Month = t2.Month;
	go

Cách 2: 
- Thực hiện truy vấn thống kê (như ở trên) và đưa kết quả vào lưu trữ trong một biến bảng
- Bổ sung các tháng còn thiếu cho biến bảng.

	declare @year int = 2017;
	declare @tblSummary table
	(
		Month int primary key,
		Revenue money default(0)
	);
	insert into @tblSummary
		select	month(o.OrderDate) as Month,
				sum(od.Quantity * od.SalePrice) as Revenue
		from	Orders as o 
				join OrderDetails as od on o.OrderId = od.OrderId
		where	year(o.OrderDate) = @year 
		group by month(o.OrderDate);

	declare @month int = 1;
	while (@month <= 12)
		begin
			if (not exists(select * from @tblSummary where Month = @month))
				insert into @tblSummary(Month) values(@month);

			set @month = @month + 1;
		end
	
	select * from @tblSummary order by Month;
	go

Bài tập 2: Cho đoạn code sau

	declare	@startYear int = 2010,
			@endYear int = 2020;

	select	year(o.OrderDate) as Year,
			count(distinct o.OrderId) as CountOfOrders,
			sum(od.Quantity * od.SalePrice) as Revenue
	from	Orders as o
			join OrderDetails as od on o.OrderId = od.OrderId
	where	year(o.OrderDate) between @startYear and @endYear
	group by year(o.OrderDate);
	go

Yêu cầu: lập trình để kết quả báo cáo phải đủ các năm từ @startYear cho đến @endYear
Cách 1

	declare	@startYear int = 2010, @endYear int = 2020;
	declare @tblYear table
	(
		Year int primary key
	);

	declare @year int = @startYear;
	while (@year <= @endYear)
		begin
			insert into @tblYear values (@year);
			set @year = @year + 1;
		end

	select	t1.Year, isNull(t2.CountOfOrders, 0)as CountOfOrders, isnull(t2.Revenue, 0) as Revenue
	from	@tblYear as t1
			left join
			(
				select	year(o.OrderDate) as Year,
			count(distinct o.OrderId) as CountOfOrders,
			sum(od.Quantity * od.SalePrice) as Revenue
		from	Orders as o
				join OrderDetails as od on o.OrderId = od.OrderId
		where	year(o.OrderDate) between @startYear and @endYear
		group by year(o.OrderDate)
			) as t2 on t1.Year = t2.Year;
	go

Cách 2

	declare	@startYear int = 2010, @endYear int = 2020;
	declare @tblSummary table
	(
		Year int primary key,
		CountOfOrders int default(0),
		Revenue money default(0)
		
	)
	insert into @tblSummary
		select	year(o.OrderDate) as Year,
			count(distinct o.OrderId) as CountOfOrders,
			sum(od.Quantity * od.SalePrice) as Revenue
		from	Orders as o
				join OrderDetails as od on o.OrderId = od.OrderId
		where	year(o.OrderDate) between @startYear and @endYear
		group by year(o.OrderDate)

	declare @year int = @startYear;
	while (@year <= @endYear)
		begin
			if (not exists(select * from @tblSummary where Year = @year))
				insert into @tblSummary(Year) values(@year);

			set @year = @year + 1;
		end
	
	select * from @tblSummary order by Year;
	go

-- Cursor
declare contro cursor for select CustomerName, Address from Customers

open contro
declare @tenKH nvarchar(255), @diachiKh nvarchar(255)
fetch next from contro into @tenKH, @diachiKH

while(@@FETCH_STATUS = 0) 
	begin
		print @tenKh
		print  @diachiKH
		print '-----------'
		fetch next from contro into @tenKH, @diachiKH		
	end
close contro
deallocate contro
go
