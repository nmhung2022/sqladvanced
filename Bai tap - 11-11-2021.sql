﻿insert into Member(UserName, Password) select t.UserName, t.Password from Import_Member as t
insert into Post 
(
	Title,
	[Content],
	MemberId,
	CountOfLikes,
	CountOfShared,
	CountOfComments
) 
select 
	t.Title,
	t.Content,
	t.MemberId,
	t.CountOfLikes,
	t.CountOfShared,
	t.CountOfComments
from Import_Post as t where  exists(select * from Member as m  where t.MemberId = m.MemberId)



-------------------------------------------------------------------------------
--  proc_MemberActionForPost_Insert: thực hiện chức năng thêm một action trong 1 post (liked or share) 
-------------------------------------------------------------------------------
if exists(select * from sys.objects where name = 'proc_MemberActionForPost_Insert')
	drop procedure proc_MemberActionForPost_Insert
go
create procedure proc_MemberActionForPost_Insert
@MemberId int,
@PostId int,
@TypeOfAction nvarchar(255),
@TimeOfAction datetime,
@Result int output
as
begin
	set nocount on
	if(@MemberId is null or @PostId is null or @TypeOfAction not in ('liked', 'shared') 
		or @TimeOfAction is null or @TimeOfAction = '')
		begin
			set @Result = -1
			return;
		end

	if exists(select * from MemberActionForPost where PostId = @PostId)
		begin
			set @Result = -3
			return
		end
	insert into MemberActionForPost(MemberId, PostId, TypeOfAction, TimeOfAction)
	values(@MemberId, @PostId, @TypeOfAction, @TimeOfAction)

	if(@@ROWCOUNT > 0) 
		set @Result = 1
	else
		set @Result = -2
end
go

--Test thủ tục
declare @Result int
exec proc_MemberActionForPost_Insert
	@MemberId = 1,
	@PostId = 102,
	@TypeOfAction = 'shared',
	@TimeOfAction = '2021-11-18',
	@Result = @Result output

select @Result



--1. Viết các trigger cho các lệnh INSERT và DELETE 
--	trên bảng MemberActionForPost sao cho khi bổ sung hay xóa dữ liệu trên bảng này, 
--	giá trị của cột CountOfLikes và CountOfShares trong bảng Post (bài viết) sẽ tăng 
--	hoặc giảm đúng bằng số lượt thích (liked) và số lượt chia sẻ (shared) 
--	của các thành viên trên bài viết tương ứng.

-------------------------------------------------------------------------------
--  trg_MemberActionForPost_Insert: thực hiện chức năng điều chỉnh tổng số lượng action khi action được thêm vào
-------------------------------------------------------------------------------
if exists(select * from sys.triggers where name = 'trg_MemberActionForPost_Insert')
	drop trigger trg_MemberActionForPost_Insert
go
CREATE TRIGGER trg_MemberActionForPost_Insert 
ON MemberActionForPost 
FOR INSERT 
AS 
 UPDATE Post 
 SET Post.CountOfLikes = 
    CASE inserted.TypeOfAction
        WHEN 'liked' THEN 1 + Post.CountOfLikes
		ELSE Post.CountOfLikes
     END,
	 
	 Post.CountOfShared = 
    CASE inserted.TypeOfAction
        WHEN 'shared' THEN 1 + Post.CountOfShared
		ELSE Post.CountOfShared
     END

FROM Post INNER JOIN inserted ON Post.PostId = inserted.PostId
WHERE Post.PostId = inserted.PostId    


-------------------------------------------------------------------------------
--  trg_MemberActionForPost_Delete: thực hiện chức năng điểu chỉnh tổng số lượng action khi action được xoá
-------------------------------------------------------------------------------
if exists(select * from sys.triggers where name = 'trg_MemberActionForPost_Delete')
	drop trigger trg_MemberActionForPost_Delete
go
CREATE TRIGGER trg_MemberActionForPost_Delete 
ON MemberActionForPost 
FOR DELETE 
AS 
 UPDATE Post 
 SET Post.CountOfLikes = 
    CASE deleted.TypeOfAction
        WHEN 'liked' THEN Post.CountOfLikes - 1 
		ELSE Post.CountOfLikes
     END,
	 Post.CountOfShared = 
    CASE deleted.TypeOfAction
        WHEN 'shared' THEN Post.CountOfShared - 1
		ELSE Post.CountOfShared
     END

FROM Post INNER JOIN deleted ON Post.PostId = deleted.PostId
WHERE Post.PostId = deleted.PostId   

--2. Viết các trigger cho các lệnh INSERT, UPDATE và DELETE 
--trên bảng Comment sao cho khi bổ sung, cập nhật hay xóa dữ liệu trên bảng này, 
--giá trị của cột CountOfComments trong bảng Post sẽ tăng hoặc giảm đúng theo 
--số lượng bài thảo luận (comment) của bài viết tương ứng.


-------------------------------------------------------------------------------
--  proc_Comment_Insert: thực hiện chức năng thêm một action trong 1 post (liked or share) 
-------------------------------------------------------------------------------
if exists(select * from sys.objects where name = 'proc_Comment_Insert')
	drop procedure proc_Comment_Insert
go
create procedure proc_Comment_Insert
@CommentContent nvarchar(255),
@PostId int,
@MemberId int,
@Result int output
as
begin
	set nocount on
	if(@MemberId is null or @PostId is null)
		begin
			set @Result = -1
			return;
		end

	insert into Comment(CommentContent, PostId, MemberId)
	values(@CommentContent, @PostId, @MemberId)

	if(@@ROWCOUNT > 0) 
		set @Result = @@IDENTITY
	else
		set @Result = -2
end
go

--Test thủ tục
--declare @Result int
--exec proc_Comment_Insert
--	@CommentContent = 'Haha',
--	@PostId = 101,
--	@MemberId = '35',
--	@Result = @Result output
--select @Result


-------------------------------------------------------------------------------
--  trg_Comment_Insert: thực hiện chức năng điều chỉnh tổng số lượng CountOfComment sau khi comment 
-- có tồn tài trong POST đã được thêm vào
-------------------------------------------------------------------------------
if exists(select * from sys.triggers where name = 'trg_Comment_Insert')
	drop trigger trg_Comment_Insert
go
CREATE TRIGGER trg_Comment_Insert 
ON Comment 
FOR INSERT 
AS 
	UPDATE Post 
	SET Post.CountOfComments = Post.CountOfComments + 1
	FROM Post JOIN inserted ON Post.PostId = inserted.PostId
	WHERE Post.PostId = inserted.PostId

------------------------------------------------------------------------------
--  trg_Comment_Insert: thực hiện chức năng điều chỉnh tổng số lượng CountOfComment sau khi comment 
-- có tồn tài trong POST đã được xoá
-------------------------------------------------------------------------------
if exists(select * from sys.triggers where name = 'trg_Comment_Delete')
	drop trigger trg_Comment_Delete
go
CREATE TRIGGER trg_Comment_Delete 
ON Comment 
FOR Delete 
AS 
	UPDATE Post 
	SET Post.CountOfComments = Post.CountOfComments - 1
	FROM Post JOIN deleted ON Post.PostId = deleted.PostId
	WHERE Post.PostId = deleted.PostId


