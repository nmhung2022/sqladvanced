﻿w--1. Thống kê xem mỗi một loại hàng có bao nhiêu mặt hàng.
SELECT p.CategoryID, c.CategoryName, COUNT(p.ProductID) AS SoMatHang
FROM Products AS p Join Categories AS c ON c.CategoryID = p.CategoryID
GROUP BY p.CategoryID, c.CategoryName

--2. Thống kê xem mỗi một khách hàng đã đặt bao nhiêu đơn đặt hàng.
SELECT c.CustomerId, c.CustomerName, COUNT(o.OrderId) AS SoDonHangDaDat
FROM Customers AS c Join Orders AS o ON c.CustomerId = o.CustomerId
GROUP BY c.CustomerId, c.CustomerName


--3. Thống kê số lượng đơn hàng mà mỗi shipper đã vận chuyển.
SELECT s.ShipperId, s.ShipperName, COUNT(o.OrderId) AS SoDonHangDaVanChuyen
FROM Shippers AS s Join Orders AS o ON s.ShipperId = o.ShipperId
GROUP BY s.ShipperId, s.ShipperName

--4. Thống kê số lượng nhà cung cấp theo từng quốc gia.
SELECT s.Country, COUNT(s.SupplierId) AS SoLuongNhaCungCap
FROM Suppliers as s
GROUP BY  s.Country


--5. Thống kê số lượng khách hàng theo từng quốc gia.
SELECT c.Country, COUNT(c.CustomerId) AS SoLuongKhacHang
FROM Customers AS c
GROUP BY  c.Country


--6. Thống kê tổng số lượng đơn hàng theo từng quốc gia của khách hàng.
SELECT c.Country, SUM(o.OrderId) AS TongSoDonHang
FROM Customers AS c Join Orders AS o On c.CustomerId = o.CustomerId
GROUP BY  c.Country


--7. Thống kê xem trong quý 4 năm 2017, mỗi nhân viên đã lập được bao nhiêu đơn đặt hàng.
SELECT e.EmployeeId, e.FirstName, COUNT(o.OrderId) AS SoLuongDonHangDaLap
FROM Employees AS e JOIN Orders AS o On e.EmployeeId = o.EmployeeId
WHERE  MONTH(o.OrderDate) >=9 And MONTH(o.OrderDate) <=12 And YEAR(o.OrderDate) =2017
GROUP BY  e.EmployeeId, e.FirstName

--8. Hãy cho biết trong thời gian từ tháng 6 đến tháng 12 năm 2017, mỗi một shipper đã nhận vận chuyển bao nhiêu đơn hàng.
SELECT s.ShipperId, ShipperName, COUNT(o.OrderId) AS SoLuongDonHangVanChuyen
FROM Shippers AS s JOIN Orders AS o On s.ShipperId = o.ShipperId
WHERE o.OrderDate Between '2017/06/01' And '2017/12/31'
GROUP BY s.ShipperId, ShipperName

--9. Thống kê số lượng đơn hàng trong năm 2017 của các khách hàng ở Mỹ (USA), Anh (UK), Đức (Germany) và Pháp (France).
SELECT c.CustomerId, c.Country, COUNT(o.OrderId) AS SoLuongDongHang
FROM Customers AS c LEFT JOIN Orders As o On c.CustomerId = o.CustomerId And YEAR(o.OrderDate) = 2017 
WHERE   c.Country IN (N'USA' ,N'UK' , N'Germany' , N'France')
GROUP BY c.CustomerId, c.Country

--10. Số tiền mà khách hàng phải thanh toán cho mỗi mặt hàng trong đơn hàng được tính theo công thức:
--		Quantity * SalePrice
--		Hãy hiển thị các thông tin sau đây của các đơn hàng được đặt trong năm 2017: 
--		Mã đơn hàng, ngày đặt hàng, thông tin của nhân viên lập đơn hàng, 
--		thông tin của khách hàng, thông tin của shipper 
--		và tổng số tiền hàng mà khách hàng phải thanh toán (tức là trị giá của đơn hàng).
SELECT o.OrderId, OrderDate, e.EmployeeId, c.CustomerName, s.ShipperName, SUM(od.Quantity * od.SalePrice) AS GiaTriDonHang
FROM ((((Customers AS c Join Orders AS o On c.CustomerId = o.CustomerId) Join Employees As e On e.EmployeeId = o.EmployeeId) 
		Join Shippers As s On s.ShipperId = o.ShipperId) Join OrderDetails As od On o.OrderId = od.OrderId)
WHERE YEAR(o.OrderDate) = 2017
GROUP BY o.OrderId, OrderDate, e.EmployeeId, c.CustomerName, s.ShipperName


--11. Thống kê tổng số lượng (được bán) và tổng doanh thu của mỗi mặt hàng trong năm 2017.
SELECT p.ProductId, ProductName, ISNULL(SUM(Quantity), 0) As SoLuongHangDaBan, ISNULL(SUM(od.SalePrice * od.Quantity), 0) As TongDoanhThu
FROM Orders As o Join OrderDetails As od On od.OrderId = o.OrderId Join Products As p On p.ProductId = od.ProductId
WHERE YEAR(o.OrderDate) = 2017
GROUP BY p.ProductId, ProductName

--13. Cho biết mã đơn hàng, ngày đặt hàng và thông tin khách hàng của những đơn hàng có tổng trị giá lớn hơn 1000$.
SELECT o.OrderId, OrderDate, c.CustomerId, CustomerName , SUM(od.SalePrice * od.Quantity) As TongDoanhThu
FROM ((Orders As o Join OrderDetails As od On od.OrderId = o.OrderId) Join Products As p On p.ProductId = od.ProductId
		Join Customers As c On c.CustomerId = o.CustomerId)
WHERE YEAR(o.OrderDate) = 2017
GROUP BY  o.OrderId, OrderDate, c.CustomerId, CustomerName 
HAVING SUM(od.SalePrice * od.Quantity) > 1000



--14. Những nhân viên nào có số lượng đơn hàng lập trong tháng 8 năm 2017 lớn 5.
SELECT o.EmployeeID, e.FirstName, LastName AS Thang, COUNT(o.OrderId) As SoLuongDonHang
FROM Employees AS e JOIN Orders AS o ON e.EmployeeID = o.EmployeeID
	JOIN OrderDetails AS od ON o.OrderID = od.OrderID
WHERE MONTH(o.OrderDate) = 8 And YEAR(o.OrderDate) = 2017 
GROUP BY o.EmployeeId, e.FirstName, LastName
HAVING COUNT(o.OrderId) > 5




--12. Hãy cho biết mỗi một quốc gia có bao nhiêu nhà cung cấp, bao nhiêu khách hàng 
--    (kết quả hiển thị bao gồm 3 cột: Country, CountOfSuppliers, CountOfCustomers).


--15. Giả sử, mức phí vận chuyển mà công ty phải chi trả cho các shipper trên mỗi đơn hàng được qui định như sau:
--	- Các đơn hàng của khách hàng tại USA và Canada: mức phí vận chuyển là 3% trị giá của đơn hàng.
--	- Các đơn hàng của khách hàng tại Argentina, Brazil, Mexico và Venezuela: mức phí vận chuyển là 5% trị giá của đơn hàng.
--	- Các đơn hàng của khách hàng ở các quốc gia khác: mức phí vận chuyển là 7% trị giá của đơn hàng.
--   Hãy cho biết mã đơn hàng, ngày đặt hàng, thông tin khách hàng, thông tin shipper, trị giá của đơn hàng và mức phí vận chuyển của mỗi đơn hàng. 


--16. Dựa vào cách tính như đã qui định ở trên, hãy cho biết tổng số tiền mà công ty phải chi trả cho mỗi shipper là bao nhiêu.

--17. Cho biết mã, tên, địa chỉ và số lượng mặt hàng của những nhà cung cấp có số lượng mặt hàng cung cấp cho công ty nhiều nhất.


--18. Trong năm 2017, những mặt hàng nào có tổng doanh thu cao nhất? Doanh thu là bao nhiêu?

--19. Trong năm 2018, những nhân viên nào đem lại doanh thu cao nhất cho công ty? Là bao nhiêu? 
--    (doanh thu mà mỗi nhân viên đem lại cho công ty được tính dựa trên tổng giá trị các đơn hàng mà nhân viên đó phụ trách).

--20. Hãy lập bảng thống kê doanh thu của mỗi mặt hàng trong năm 2017, kết quả truy vấn được hiển thị theo mẫu sau đây:
--		ProductId	ProductName	Jan	Feb	Mar	Apr	May	Jun	Jul	Aug	Sep	Oct	Nov	Dec
--		...			....		...	...	...	...	...	...	...	...	...	...	...	...
--		...			....		...	...	...	...	...	...	...	...	...	...	...	...
