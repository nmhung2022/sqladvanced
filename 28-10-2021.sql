﻿insert into CardStore 
(
	CardTypeId,
	Serial,
	PinNumber,
	Amount,
	ExpiredTime,
	CardStatus,
	InvoiceId
) 
select 
	t.CardTypeId, 
	t.Serial, 
	t.PinNumber, 
	t.Amount,
	t.ExpiredTime, 
	1, 
	NULL 
from Import_CardStore_20211128 as t
where not exists(select * from CardStore 
				where CardTypeId = t.CardTypeId and Serial = t.Serial )


--1. Viết các thủ tục xử lý dữ liệu cơ bản liên quan đến khách hàng (Customer) - proc_Customer_Register: Đăng ký một tài khoản khách hàng mới: 

-------------------------------------------------------------------------------
--  proc_Customer_Register: Đăng ký một tài khoản khách hàng mới
-------------------------------------------------------------------------------
--Trong đó, @CustomerId trả về 1 giá trị theo qui ước: 
--▪ Nếu thành công: @@IDENTITY của khách hàng vừa được đăng ký 
--▪ Nếu không thành công: Trả về 1 giá trị số âm theo qui ước:
--2 
--∙ -1 : Email bị trùng/không hợp lệ 
--∙ -2 : Password rỗng 
--∙ -3 : Tên khách hàng rỗng 
--▪ -4 : MobiNumer không hợp lệ 

if(exists(select *  from sys.objects where name = 'proc_Customer_Register'))
	drop procedure  proc_Customer_Register
go	
create procedure proc_Customer_Register
@CustomerName nvarchar(255),
@Email nvarchar(255),
@Password nvarchar(255),
@MobiNumber nvarchar(50),
@CustomerId int OUTPUT 
as
	begin
		set nocount on;
		if((@CustomerName is null) or @CustomerName = N'')
			begin
				set @CustomerId = -3
				return;
			end
		if(@Password = N'' or LEN(@Password) < 8)
			begin
				set @CustomerId = -2
				return;
			end
		if (not exists(select * from Customer where Email = @Email) or @Email = '' or  @Email NOT LIKE '%_@__%.__%' AND PATINDEX('%[^a-z,0-9,@,.,_,\-]%', @Email) = 0)
			begin
				set @CustomerId = -1
				return;
			end
		if(@MobiNumber = NULL or LEN(@MobiNumber) < 10)
			begin
				set @CustomerId = -4
				return;
			end

		set @CustomerId = SCOPE_IDENTITY();
		insert into Customer (CustomerName, Email, Password, MobiNumber)
		values (@CustomerName, @Email, @Password, @MobiNumber);
		

	end
go


-- Test thủ tục: proc_Categories_Insert
declare @CustomerId int;
exec  proc_Customer_Register
		@CustomerName = N'Nguyễn Văn A',
		@Email = N'nguyenvana@gmail.com',
		@Password = 'avc',
		@MobiNumber = '1202211',
		@CustomerId = @CustomerId output

if(@CustomerId > 0) 
	select * from Customer
if(@CustomerId = -1) 
	select @CustomerId as 'Error Code', N'Email bị trùng/không hợp lệ ' as N'Error Message';
if(@CustomerId = -2) 
	select @CustomerId as 'Error Code', N' Password rỗng or qúa ngắn' as N'Error Message';
if(@CustomerId = -3) 
	select @CustomerId as 'Error Code', N'Tên khách hàng rỗng ' as N'Error Message';
if(@CustomerId = -4) 
	select @CustomerId as 'Error Code', N'MobiNumer không hợp lệ ' as N'Error Message';

go




-------------------------------------------------------------------------------
--  proc_Customer_ChangePwd: thực hiện chức năng đổi mật khẩu của khách hàng 
-------------------------------------------------------------------------------
--Tham số: 
--@Email 
--@OldPassword 
--@NewPassword 
--@Result int OUTPUT 
--Trả về kết quả: 
--1: thành công 
--0: không thành công 

if(exists(select * from sys.objects where name = 'proc_Customer_ChangePwd'))
	drop procedure proc_Customer_ChangePwd
go

create procedure proc_Customer_ChangePwd
@Email nvarchar(50),
@OldPassword nvarchar(50),
@NewPassword nvarchar(50),
@Result int OUTPUT 
as 
begin 
	set nocount on;
	if((@Email is null) or @Email = '' or  @Email NOT LIKE '%_@__%.__%' AND PATINDEX('%[^a-z,0-9,@,.,_,\-]%', @Email) = 0)
		begin 
			set @Result = 0
			return;
		end
	if(not exists(select Password from Customer where Password = @OldPassword))
		begin
			set @Result = 0
			return;
		end

	update Customer
	set Password = @NewPassword
	Where Email = @Email

	if(@@ROWCOUNT) > 1 
		set @Result = 1;
	else
		set @Result = 0;
end
go

-- Test thủ tục:
declare @Result bit;
exec proc_Customer_ChangePwd
	@Email = 17,
	@OldPassword = '12345',
	@NewPassword = N'12345',
	@Result = @Result output;


if(@Result = 1) 
	select * from Customer 
if(@Result = 0) 
	select @Result as 'Error Code', N'Không thành công ' as N'Error Message';

-------------------------------------------------------------------------------
--- proc_Customer_Authenticate: Dùng để kiểm tra thông tin đăng nhập của khách hàng
-------------------------------------------------------------------------------
--Tham số: 
--@Email 
--@Password 
--Kết quả trả về: 
--▪ Nếu đăng nhập thành công, thủ tục trả về thông tin của tài khoản 

if(exists(select * from sys.objects where name = 'proc_Customer_Authenticate'))
	drop procedure proc_Customer_Authenticate
go

create procedure proc_Customer_Authenticate
@Email nvarchar(55),
@Password nvarchar(20)
as
begin 
	declare @id int;
	set nocount on
	if(not exists(select 1 from Customer where Email = @Email))
		return;
	else if(not exists(select 1 from Customer where Password = @Password))
		return;
	else
		select top 1 * from Customer where Email = @Email
end
go

-- Test thủ tục:
exec proc_Customer_Authenticate
	@Email = 'ecurbishley132@fc2.com',
	@Password = N'gsN9Cofn'

-------------------------------------------------------------------------------
--- proc_Customer_Update: thực hiện chức năng cập nhật thông tin tài khoản khách hàng 
-------------------------------------------------------------------------------
--Tham số: 
--@CustomerId 
--@CustomerName 
--@Email 
--@MobiNumber 

if(exists(select * from sys.objects where name = 'proc_Customer_Update'))
	drop procedure proc_Customer_Update
go
create procedure proc_Customer_Update
@CustomerId bigint,
@CustomerName nvarchar(255), 
@Email nvarchar(50),
@MobiNumber nvarchar(20)
as
begin 
	set nocount on;
	if((@CustomerId is null) or (@CustomerName is null) or (@Email is null) or (@MobiNumber is null))
		return;
	else if (not exists(select * from Customer where CustomerId = @CustomerId))
		return;
	update Customer
	set CustomerId = @CustomerId,
		CustomerName = @CustomerName,
		Email = @Email,
		MobiNumber = @MobiNumber
	where Email = @Email
	select * from Customer where Email = @Email

end
go

-- Test thủ tục:
exec proc_Customer_Update
	
	@CustomerId = 12,
	@CustomerName = N'Hhha',
	@Email = 'ecurbishley13@fc2.com',
	@MobiNumber = '2312412412'


-------------------------------------------------------------------------------
--- proc_Customer_Clean: Thực hiện việc khóa các tài khoản khách hàng đã đăng ký trong  
--- vòng 1 năm nhưng chưa thực hiện bất kỳ một giao dịch mua thẻ nào
-------------------------------------------------------------------------------

if(exists (select * from sys.objects where name = 'proc_Customer_Clean'))
	drop procedure proc_Customer_Clean
go

create procedure proc_Customer_Clean
@Result int OUTPUT 

as 
begin
	set nocount on
	
	update c
	set IsLocked = 1
	from Customer as c
	where (datediff(year, c.RegisterTime, GETDATE()) >= 365)
		and (not exists(select * from Invoice as i where c.CustomerId = i.CustomerId))	
end
go
-- Test thủ tục:
declare @Result bit;
exec proc_Customer_Clean
	@Result = @Result output;


--- proc_Customer_Select: Hiển thị danh sách khách hàng. Kết quả truy vấn được phân  trang và sắp xếp theo tên của khách hàng. 
--Tham số: 
--@SearchValue nvarchar(255) = N’’ 
--@Page int -- Trang cần xem 
--@PageSize int -- Số dòng dữ liệu trên mỗi trang 
--@RowCount int OUTPUT -- Tổng số dòng dữ liệu truy vấn được 
--@PageCount int OUTPUT -- Tổng số trang 
--Lưu ý: 
--o Nếu tham số @SearchValue khác rỗng thì có thể tìm kiếm tương đối theo tên,  email và số di động của khách hàng 
if(exists(select * from sys.objects where name = 'proc_Customer_Select'))
	drop procedure proc_Customer_Select
go 
create procedure proc_Customer_Select
@SearchValue nvarchar(255) = N'',
@Page int,
@PageSize int,
@RowCount int OUTPUT,
@PageCount int OUTPUT
as
begin 
	set nocount on;
	if(@Page <= 0) set @Page = 1
	if(@PageSize <= 0) set @PageSize = 20
	select *, ROW_NUMBER() over(order by CustomerName desc) as RowNumber
	into #TempCustomer
	from Customer as c
	where (@SearchValue = N'' or (c.CustomerName = @SearchValue)) and
		((CustomerName like @SearchValue) or (Email like @SearchValue) or MobiNumber like @SearchValue)
		

	set @RowCount = @@ROWCOUNT;
    set @PageCount = @RowCount / @PageSize;    
    if (@RowCount % @PageSize > 0)
        set @PageCount += 1;
	 select  *
    from    #TempCustomer
    where   RowNumber between (@Page - 1) * @PageSize + 1 and @Page * @PageSize 
    order by RowNumber;
end
go

-- Test thủ tục
declare @rowCount int,
        @pageCount int;
exec proc_Customer_Select
	@SearchValue = N'',
	@Page = 1,
	@PageSize = 20,
	@RowCount = @rowCount OUTPUT,
	@PageCount = @pageCount OUTPUT

select @rowCount as [RowCount], @pageCount as [PageCount];

-------------------------------------------------------------------------------
--- proc_CardType_Insert Thêm mới một CardType
-------------------------------------------------------------------------------

if(exists(select * from sys.objects where name = 'proc_CardType_Insert'))
	drop procedure proc_CardType_Insert
go 
create procedure proc_CardType_Insert
@CardTypeId int,
@CardTypeName varchar(10),
@Descriptions nvarchar(255),
@Result bit output

as
begin
	set nocount on
	if(@CardTypeName is null) or (@Descriptions is null) or (@CardTypeId is null)
		begin
			set @Result = 0
			return
		end
	if(exists(select * from CardType as c where c.CardTypeId = @CardTypeId))
		begin
			set @Result = 0
			return
		end
	insert CardType(CardTypeId, CardTypeName, Descriptions)	
	values(@CardTypeId, @CardTypeName, @Descriptions)
	set @Result = 1
end
go

-- Test thủ tục:
declare @Result bit;
exec proc_CardType_Insert
	@CardTypeId = 4,
	@CardTypeName = 'Hihi',
	@Descriptions = 'haha',
	@Result = @Result output

if(@Result = 1) 
	print('Success')
else
	print('Fail')

-------------------------------------------------------------------------------
--- proc_CardType_Update Cấp nhật một CardType theo CardTypeId
-------------------------------------------------------------------------------
if(exists(select * from sys.objects where name = 'proc_CardType_Update'))
	drop procedure proc_CardType_Update
go 
create procedure proc_CardType_Update
@CardTypeId int,
@CardTypeName varchar(10),
@Descriptions nvarchar(255),
@Result bit output

as
begin
	set nocount on
	if(@CardTypeId is null)
		begin
			set @Result = 0
			return
		end	
	update CardType
	set 
		CardTypeName =  @CardTypeName,
		Descriptions = @Descriptions,
		CardTypeId = @CardTypeId

	if(@@ROWCOUNT) > 1 
		set @Result = 1;
	else
		set @Result = 0;

end
go

-- Test thủ tục:
declare @Result bit;
exec proc_CardType_Update
	@CardTypeId = 4,
	@CardTypeName = 'Hihi new',
	@Descriptions = 'Haha new',
	@Result = @Result output

if(@Result = 1) 
	print('Success')
else
	print('Fail')

select * from CardType


-------------------------------------------------------------------------------
--- proc_Invoice_Select: Cấp nhật một CardType theo CardTypeId
--Hiển thị danh sách các hóa đơn mua thẻ. Kết quả truy vấn được  phân trang và sắp xếp theo thời gian mua thẻ. 
--Tham số đầu vào: 
--@CustomerId -- Bằng rỗng nếu lấy của tất cả khách hàng 
--@FromDate -- Bằng NULL nếu không giới hạn thời gian bắt đầu 
--@ToDate -- Bằng NULL nếu không giới hạn thời gian kết thúc 
--@Page 
--@PageSize 
--@RowCount OUTPUT 
--@PageCount OUTPUT 
-------------------------------------------------------------------------------
if (exists(select * from sys.objects where name = 'proc_Invoice_Select'))
    drop procedure proc_Invoice_Select
go

create procedure proc_Invoice_Select
    @CustomerId int = 0,
    @FromDate datetime = null,
    @ToDate datetime = null,
    @Page int = 1,
    @PageSize int = 20,
    @RowCount int output,
    @PageCount int output
as
begin
    set nocount on;

    if (@Page <= 0) set @Page = 1;
    if (@PageSize <= 0) set @PageSize = 20;

    -- Truy vấn dữ liệu đưa vào bảng tạm
    select  *,
            row_number() over(order by CreatedTime desc) as RowNumber
    into    #TempInvoice
    from    Invoice as i
    where   (
                (@CustomerId = 0) 
             or (i.CustomerId = @CustomerId)
            )
			and (
					((@FromDate is null) and (@ToDate is null))
				 or ((@FromDate is null) and (i.CreatedTime <= @ToDate))
				 or ((@ToDate is null) and (i.CreatedTime >= @FromDate))
				 or (i.CreatedTime between @FromDate and @ToDate)
				);

    -- Tính số dòng và số trang
    set @RowCount = @@ROWCOUNT;
    set @PageCount = @RowCount / @PageSize;    
    if (@RowCount % @PageSize > 0)
        set @PageCount += 1;

    -- Hiển thị dữ liệu của trang cần xem
    select  *
    from    #TempInvoice
    where   RowNumber between (@Page - 1) * @PageSize + 1 and @Page * @PageSize 
    order by RowNumber;
end
go

-- Test thủ tục
declare @rowCount int,
        @pageCount int;

exec proc_Invoice_Select
        @CustomerId = 0,
        @FromDate = '2021/01/01',
        @ToDate = '2021/01/09',
        @Page = 1,
        @PageSize = 20,
        @RowCount = @rowCount output,
        @PageCount = @pageCount output;

select @rowCount as [RowCount], @pageCount as [PageCount];
-------------------------------------------------------------------------------
-- proc_CardStore_ShowStatistics: Thực hiện chức năng thống kê số lượng thẻ trong kho,  
--kết quả được hiển thị theo mẫu sau: 
--Loại thẻ -- Mệnh giá --Tổng số lượng thẻ -- Số lượng thẻ đã bán -- Số lượng thẻ còn tồn -- Số lượng thẻ bị khóa hoặc  hết hạn
-------------------------------------------------------------------------------
-- proc_CardStore_ShowStatistics
if (exists(select * from sys.objects where name = 'proc_CardStore_ShowStatistics'))
    drop procedure proc_CardStore_ShowStatistics
go

create procedure proc_CardStore_ShowStatistics    
as
begin
    set nocount on;
    select  t1.CardTypeName as N'Loại thẻ',
            t2.Amount as N'Mệnh giá',
            count(t2.CardId) as N'Tổng số lượng thẻ',
            count(case when t2.CardStatus = 0 then t2.CardId else null end) as N'Tổng số lượng thẻ đã bán',
            count(case when t2.CardStatus = 1 then t2.CardId else null end) as N'Tổng số lượng thẻ còn tồn',
            count(case 
                        when t2.CardStatus = -1 or t2.ExpiredTime < getdate() then t2.CardId
                        else null
                  end) as N'Tổng số lượng thẻ bị khóa hoặc hết hạn'
    from    CardType as t1
            join CardStore as t2 on t1.CardTypeId = t2.CardTypeId
    group by t1.CardTypeId, t1.CardTypeName, t2.Amount
    order by t1.CardTypeId, t2.Amount;
end
go

-- Test thủ tục:
proc_CardStore_ShowStatistics


-------------------------------------------------------------------------------
--- proc_Customer_Summary: thống kê tổng số lượng tiền mà mỗi khách hàng đã bỏ ra để mua thẻ trong một khoảng thời gian nào đó 
--Tham số đầu vào: 
--@FromDate -- Bằng NULL nếu không giới hạn thời gian bắt đầu 
--@ToDate -- Bằng NULL nếu không giới hạn thời gian kết thúc 
-------------------------------------------------------------------------------
if (exists(select * from sys.objects where name='proc_Customer_Summary')) 
drop procedure proc_Customer_Summary
go
create proc proc_Customer_Summary
(
    @FromDate date,
    @ToDate date
)
as
begin
    set nocount on;
	
    select sum(ct.Amount) as Renuven from Customer as c
    join Invoice as i on c.CustomerID = i.CustomerId
    join CardStore as ct on ct.InvoiceId = i.InvoiceID
    where i.CreatedTime between @FromDate and @ToDate
    group by i.InvoiceID
end
go

execute proc_Customer_Summary
    @FromDate ='07/12/2020',
    @ToDate = '10/12/2020'

-------------------------------------------------------------------------------
--- func_GetInventories: Hàm có chức năng tính số lượng thẻ đang còn tồn trong kho (tức là  thẻ chưa bán) theo loại thẻ và mệnh giá 
--Tham số đầu vào: 
--@CardTypeId 
--@Amount 
-------------------------------------------------------------------------------
if (exists(select * from sys.objects where name='func_GetInventories')) 
drop procedure func_GetInventories
go
create proc func_GetInventories
(
    @CardTypeId int,
    @Amount money
)
as
begin
    set nocount on;
    select *
    from(
        select * from CardStore as c
        where c.Amount = @Amount and c.CardTypeId = @CardTypeId
    ) as p
    where p.CardStatus = 1
    
end 
go

-- Test thủ tục:
exec func_GetInventories
	@CardTypeId = 1,
	@Amount = 2000.2



-------------------------------------------------------------------------------
--- func_GetRevenueByDate: Hàm cho biết doanh thu (số tiền bán thẻ) hàng ngày . Tham số đầu vào: 
--@CardTypeId -- Bằng 0 nếu thống kê theo tất cả các loại thẻ 
--@FromDate 
--@ToDate 
-------------------------------------------------------------------------------

