﻿--Câu 1
--a. proc_AssignProjectToEmployee 
--@EmployeeId int, 
--@ProjectId int 
--@AssignedDate date, 
--@Message nvarchar(50) output 
--Có chức năng bổ sung một nhân viên cho dự án. Trong trường hợp đầu vào không hợp lệ, tham số đầu ra @Message trả về chuỗi thông báo lý do. 
-- Trường hợp bổ sung thành công, tham số @Message trả về chuỗi rỗng.

if(exists(select * from sys.objects where name = 'proc_AssignProjectToEmployee'))
	drop procedure proc_AssignProjectToEmployee
go

create procedure proc_AssignProjectToEmployee
@EmployeeId int, 
@ProjectId int,
@AssignedDate date, 
@Message nvarchar(50) output
as
begin
	set nocount on
	if(not exists(select * from Employee where EmployeeId = @EmployeeId))
		begin
			set @Message = 'EmployeeId is not exists'
		end
	if(not exists(select * from Project where ProjectId = @ProjectId))
		begin
			set @Message = 'ProjectId is not exists'
		end
	if(@EmployeeId is null or @ProjectId is null)
		begin
			set @Message = 'EmployeeId or ProjectId is required'
			return
		end
	insert into Assignment(EmployeeId, ProjectId, AssignedDate)
	values(@EmployeeId, @ProjectId, @AssignedDate)

	if(@@ROWCOUNT > 0)
		begin
			set @Message = ''
			return
		end
	set @Message = 'Error'
end
go

--Test thủ tục
declare @Message nvarchar(50)
exec proc_AssignProjectToEmployee
	@EmployeeId = 2, 
	@ProjectId = 1,
	@AssignedDate  = '12/09/2021', 
	@Message = @Message output

select @Message


--proc_FinishAssignedProject 
--@EmployeeId int, 
--@ProjectId int 
--@FinishedDate date, 
--@Result int output
--Có chức năng cập nhật thời điểm hoàn thành dự án (FinishedDate) của một nhân viên. 
-- Yêu cầu:  chỉ cho phép cập nhật nếu @FinishedDate khác NULL và thời điểm hoàn thành phải sau thờ điểm được giao (AssignedDate). 
--Tham số đầu ra @Result trả về giá trị là 1 nếu thao tác cập  nhật thành công, ngược lại thì trả về giá trị nhỏ hơn hoặc bằng 0.

if exists(select * from sys.objects where name = 'proc_FinishAssignedProject')
	drop proc proc_FinishAssignedProject
go

create procedure proc_FinishAssignedProject
@EmployeeId int, 
@ProjectId int,
@FinishedDate date, 
@Result int output
as
begin
	set nocount on
	update Assignment
	set FinishedDate = @FinishedDate
	from Assignment as a
	where a.EmployeeId = @EmployeeId and ProjectId = @ProjectId and FinishedDate is null and datediff(day, a.AssignedDate, @FinishedDate) > 0

	if(@@ROWCOUNT > 0)
		begin
			set @Result = 1
			return
		end
	set @Result = 0
end
go

--Test thủ tục
declare @Result int
exec proc_FinishAssignedProject
@EmployeeId = 2, 
@ProjectId = 1,
@FinishedDate = '12/09/2022', 
@Result = @Result output
select @Result


--proc_ListEmployees 
--@SeachValue nvarchar(255) = N’’, 
--@Page int = 1, 
--@PageSize int = 20, 
--@RowCount int output, 
--@PageCount int output 
--Có chức năng hiển thị và tìm kiếm danh sách nhân viên dưới dạng phân trang dữ liệu. 
-- Trong đó, @SeachValue là họ tên của nhân viên cần tìm (tìm kiếm tương đối, 
-- nếu @SeachValue bằng rỗng thì không tìm kiếm theo tên) @Page là trang cần hiển thị, 
-- @PageSize là số dòng dữ liệu  được hiển thị trên mỗi trang, tham số đầu ra @RowCount 
-- cho biết tổng số dòng dữ liệu và tham số đầu ra @PageCount cho biết tổng số trang. 


if exists(select * from sys.objects where name = 'proc_ListEmployees')
	drop proc proc_ListEmployees
go

create procedure proc_ListEmployees
@SeachValue nvarchar(255) = N'', 
@Page int = 1, 
@PageSize int = 20, 
@RowCount int output, 
@PageCount int output
as
begin	
	set nocount on

	if(@Page < 0) set @Page = 1
	if(@PageSize < 0) set @PageSize = 20

	if (@SeachValue <> N'')
		set @SeachValue = '%' + @SeachValue + '%';


	select	e.*, ROW_NUMBER() over(order by(e.FullName)) as RowNumber
	into	#TempEmployee
	from	Employee as e	
	where	FullName like @SeachValue


	set @RowCount = @@ROWCOUNT;

	
	select	*
	from	#TempEmployee
	where	RowNumber between (@Page - 1) * @PageSize + 1 and @Page * @PageSize;

end
go

--Test thủ tục
declare @RowCount int
declare @PageCount int
exec proc_ListEmployees
@SeachValue = N'H', 
@Page = 1, 
@PageSize = 10, 
@RowCount = @RowCount output, 
@PageCount = @PageCount output

select @RowCount as RowCount1, @PageCount as PageCount

--d. proc_CountProjectsByMonth 
--@Year int 
--Có chức năng thống kê số lượng dự án trong mỗi tháng của năm @Year. Yêu cầu kết quả thống kê  phải đầy đủ các tháng trong năm.

if exists(select * from sys.objects where name = 'proc_CountProjectsByMonth')
	drop proc proc_CountProjectsByMonth
go

create procedure proc_CountProjectsByMonth
@Year int
as
begin
	set nocount on
		declare @tblSummary table
	(
		Month int primary key,
		CountOfProjects int default(0)
	);

	insert into @tblSummary
		select	month(p.StartDate) as Month,
				count(p.ProjectId) as CountOfProjects
		from	Project as p
		where	year(p.StartDate) = @year 
		group by month(p.StartDate);

	declare @month int = 1;
	while (@month <= 12)
		begin
			if (not exists(select * from @tblSummary where Month = @month))
				insert into @tblSummary(Month) values(@month);

			set @month = @month + 1;
		end	
	select * from @tblSummary order by Month;
end
go

--Test thu tuc
exec proc_CountProjectsByMonth
@year = 2021


--Câu 2
--a. trg_Assignment_Insert: có chức năng bắt lệnh INSERT trên bảng Assignment 
-- sao cho mỗi khi bổ sung dữ liệu cho bảng này thì tính lại giá trị của cột CountOfAssigned 
-- trong bảng Project bằng đúng với số lượng nhân viên đã được phân công thực hiện dự án. 

if exists(select * from sys.triggers where name = 'trg_Assignment_Insert')
	drop trigger trg_Assignment_Insert
go

create trigger trg_Assignment_Insert
on Assignment for insert
as

begin
	set nocount on
	update p
	set p.CountOfAssigned = a.CountOfEmployeeAssigned
	from Project as p join
	(
		select count(t.EmployeeId) as CountOfEmployeeAssigned, a.ProjectId
		from inserted as t join Assignment as a on a.ProjectId = t.ProjectId
		group by t.EmployeeId, a.ProjectId

	) as a on p.ProjectId = a.ProjectId
end
go

--Test trigger
select * from Project where ProjectId = 1
insert into Assignment(ProjectId, EmployeeId, AssignedDate) values(1, 5, '2021-12-20')
select * from Project where ProjectId = 1



--trg_Assignment_Update_FinishedDate: có chức năng bắt lệnh UPDATE trên bảng  Assignment sao cho 
--  khi cập nhật giá trị của cột FinishedDate trong bảng này thì tính lại giá trị của cột CountOfFinished 
-- bằng đúng với số lượng nhân viên đã hoàn thành  thành dự án (lưu ý: 
--một nhân viên sẽ được xem là hoàn thành dự án nếu giá trị trường  FinishedDate là khác NULL). 

if exists(select * from sys.triggers where name = 'trg_Assignment_Update_FinishedDate')
	drop trigger trg_Assignment_Update_FinishedDate
go

create trigger trg_Assignment_Update_FinishedDate
on Assignment for update
as
begin
	set nocount on

	if(update(FinishedDate))
	begin			
		update p
		set p.CountOfFinished = a.CountOfEmployeeFinshed
		from Project as p join
		(
			select count(t.EmployeeId) as CountOfEmployeeFinshed, a.ProjectId
			from inserted as t join Assignment as a on t.ProjectId = a.ProjectId
			where a.FinishedDate is not null
			group by t.EmployeeId, a.ProjectId
		) as a on p.ProjectId = a.ProjectId
	end
end

--Test trigger
select * from Project where ProjectId = 1

update Assignment
set FinishedDate = '2021-12-25'
where ProjectId = 1 and EmployeeId = 3

select * from Project where ProjectId = 1


--Câu 3
--a. func_CountAssignedEmployees(@ProjectId int) 
--Có chức năng tính số nhân viên đã được phân công thực hiện cho cho dự án có mã @ProjectId


if(exists(select * from sys.objects where name = 'func_CountAssignedEmployees'))
	drop function func_CountAssignedEmployees
go
create function func_CountAssignedEmployees(@ProjectId int)
returns table
as
return 
		select t.CountOfEmployee
		from Project as p join
		(
			select a.ProjectId, count(a.EmployeeId) as CountOfEmployee
			from Assignment as a
			group by a.ProjectId
		) as t on p.ProjectId = t.ProjectId
		where p.ProjectId = @ProjectId

go

select * from func_CountAssignedEmployees(1)


--b. func_GetAssignedEmployees(@ProjectId int) 
--Trả về một bảng cho biết danh sách mã nhân viên, họ tên, địa chỉ, ngày bắt đầu làm việc, 
-- thời điểm  được phân công và thời điểm đã hoàn thành 
-- của các nhân viên được phân công thực hiện dự án có  mã là @ProjectId.

if(exists(select * from sys.objects where name = 'func_GetAssignedEmployees'))
	drop function func_GetAssignedEmployees
go
create function func_GetAssignedEmployees(@ProjectId int)
returns table
as
return 
		select e.EmployeeId,FullName, Address, StartDate, a.AssignedDate, a.FinishedDate
		from Project as p join Assignment as a on p.ProjectId = a.ProjectId join Employee as e on e.EmployeeId = a.EmployeeId
		where p.ProjectId = @ProjectId

go

select * from func_GetAssignedEmployees(1)
