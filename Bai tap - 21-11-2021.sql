﻿

IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'trg_BangLuong_insert')
      DROP TRIGGER dbo.trg_BangLuong_insert;
go

CREATE TRIGGER trg_BangLuong_insert 
ON BangLuong 
FOR INSERT 
AS 

begin
	set nocount on; 
	Insert into BangLuongNhanVien(Nam, Thang, MaNhanVien, LuongCoBan, PhuCap, Thuong, Phat)
	Select Nam, Thang, MaNhanVien, LuongCoBan, 0, 0, 0 
	from inserted join NhanVien as nv 
	on nv.NgayBatDauHuongLuong <= EOMONTH(datefromparts(inserted.Nam, inserted.Thang, 1)) 
end
go

---Test Trigger trg_BangLuong_insert
insert into BangLuong(Thang, Nam, NgayLapBangLuong)
values(11, 2021, '2021-11-01')




IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'trg_SoGhiPhatNhanVien_insert')
      DROP TRIGGER dbo.trg_SoGhiPhatNhanVien_insert;
go

CREATE TRIGGER trg_SoGhiPhatNhanVien_insert 
ON SoGhiPhatNhanVien FOR INSERT 
AS 

begin
	set nocount on; 
	update t1
	set t1.Phat = t1.Phat + t2.TongTienPhat
	from  BangLuongNhanVien as t1 join
	(
		select YEAR(NgayPhat) as Nam, MONTH(NgayPhat) as Thang, MaNhanVien, SUM(TienPhat) as TongTienPhat
		from inserted
		group by YEAR(NgayPhat), MONTH(NgayPhat), MaNhanVien
	) as t2 on t1.Nam = t2.Nam and t1.Thang = t2.Thang and t1.MaNhanVien = t2.MaNhanVien
	
end
go
---Test Trigger trg_SoGhiPhatNhanVien_insert
insert into  SoGhiPhatNhanVien
values('NV1', '2021-11-12', N'Đi muộn', 100)





IF EXISTS (SELECT * FROM sys.triggers WHERE name = 'trg_SoGhiPhatNhanVien_update')
      DROP TRIGGER dbo.trg_SoGhiPhatNhanVien_update;
go

CREATE TRIGGER trg_SoGhiPhatNhanVien_update 
ON SoGhiPhatNhanVien FOR Update 
AS 

begin
	set nocount on; 
	update t1
	set t1.Phat = t1.Phat + t2.TongTienPhat
	from  BangLuongNhanVien as t1 join
	(
		select YEAR(NgayPhat) as Nam, MONTH(NgayPhat) as Thang, MaNhanVien, SUM(TienPhat) as TongTienPhat
		from inserted
		group by YEAR(NgayPhat), MONTH(NgayPhat), MaNhanVien
	) as t2 on t1.Nam = t2.Nam and t1.Thang = t2.Thang and t1.MaNhanVien = t2.MaNhanVien
	
end
go

-- Viết các trigger cho bảng SoGhiPhatNhanVien sao cho khi thay đổi số tiền phạt hoặc khi xoá tiền phạt thì 
-- phải tính lại số tiền phạt trong bảng lương của nhân viên cho đúng

select * from BangLuong
select * from BangLuongNhanVien
select * from NhanVien
	